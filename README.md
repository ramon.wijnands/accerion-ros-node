# Initial setup

### Cloning the API, building it and adding it to API_PATH

For Ubuntu:

Clone the API found at https://gitlab.com/accerion/accerionsensorapi.git remember the path (e.g. /home/user/accerionsensorapi/)
Create the build directory at /home/user/accerionsensorapi/build/
Go to that directory and run 
```bash
cmake .. && make
```
Add the path you remembed to the environment variables by running:
```bash
export API_PATH="/home/user/accerionsensorapi/"
```
or adding it to the /etc/environment file.

For Windows:

Clone the API found at https://gitlab.com/accerion/accerionsensorapi.git remeber the path (e.g. C:/user/accerionsensorapi/)
Create the build directory at /home/user/accerionsensorapi/build/
Go to that directory and run:
```bash
'cmake -G "MinGW Makefiles" -DWINDOWS=ON ..'
C:\Qt\Qt5.12.4\Tools\mingw730_32\bin\mingw32-make.exe -j12
```
Add the path you remembed to the environment variables by running:
```bash
set "API_PATH=%currDIR%/accerionsensorapi/"
```

### Adding accerion-ros-node into an existing catkin workspace
Clone this repository into the `src/` folder of your catkin workspace and build your packages.

### Adding accerion-ros-node into a new catkin workspace
If you are going to create a catkin workspace from scratch, do the following.

- Create a new catkin workspace folder in your home folder and navigate to it: 
```bash
mkdir ~/catkin_ws
cd ~/catkin_ws
```

- Navigate into your catkin workspace and create a folder named `src` and navigate to it: 
```bash
mkdir src
cd src
```

- Clone this repository into the `src/` folder of your catkin workspace
```bash 
git clone https://gitlab.com/accerion/accerion-ros-node.git
```

- Navigate to the main folder of your workspace and build 
```bash
cd ~/catkin_ws
catkin_make
```

- Make sure that the build completes without errors.
- Next to the `src/` folder, new folders `build/` and `devel/` should have been created in the previous step.
- Source the `devel/setup.bash` file to include this workspace in your environment
```bash
source devel/setup.bash
```
- Alternative to the previous step, you can also add the following line to your `~/.bashrc` to source your workspace at each terminal start (replace `your-user-name` with your actual username)
```bash
source /home/your-user-name/catkin_ws/devel/setup.bash
```
Then just reopen a terminal and continue to next step, or in your current terminal, do:
```bash
source ~/.bashrc
```

- To verify that your setup is complete, do:
```bash
rospack find accerion_driver
```
The response should be:
```bash
/home/your-user-name/codes/catkin_ws/src/accerion-ros-node/accerion_driver
```

# Sample launch

If you have an Accerion sensor running in the same network as your ROS master, do:

`roslaunch accerion_driver demo.launch`

to bring up the Accerion ROS Driver and RViz visualization for a sample demonstration usage.

If you have multiple Accerion sensors in your local network, you need to specify the serial number of the sensor by using the `serial_number` argument as:

`roslaunch accerion_driver demo.launch serial_number:=598100101`

Note: If you do not specify the `serial_number`, it will be assigned `0` by default. As a result, the driver will pair with the first Accerion sensor it detects in the network.
