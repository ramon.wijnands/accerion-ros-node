/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef VISUALIZATION_H
#define VISUALIZATION_H

#include <ros/ros.h>
#include <std_msgs/Float32.h>
#include <geometry_msgs/TwistStamped.h>
#include <nav_msgs/Odometry.h>
#include <rosgraph_msgs/Log.h>
#include <visualization_msgs/MarkerArray.h>
#include <geometry_msgs/PolygonStamped.h>
#include "utils.h"

class Visualization
{
public:
    Visualization();

private:

    void callbackUncorrectedOdometry(const nav_msgs::Odometry::ConstPtr& odomMsg);
    void callbackCorrectedPose(const nav_msgs::Odometry::ConstPtr& poseMsg);
    void callbackRosoutAgg(const rosgraph_msgs::Log::ConstPtr& outputLog);
    
    ros::NodeHandle nh_; 
    ros::NodeHandle* pnh_;
    std::string nodeNs_;
    std::vector<std::string> consoleOutputLines_;
    unsigned int nLinesToKeep_;

    geometry_msgs::TwistStamped outputVelocityMsg_;
    ros::Publisher              pubOutputVelocity_;
    ros::Publisher              pubOutputLinVelocity_;
    ros::Publisher              pubOutputAngVelocity_;
    ros::Publisher              pubConsoleOutput_, pubSearchRanges_;

    ros::Subscriber subsUncorrectedOdometry_, subsCorrectedPose_;
    ros::Subscriber subsRosoutAgg_;

};

#endif // VISUALIZATION_H
