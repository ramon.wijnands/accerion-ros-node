/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef UTILS_H_
#define UTILS_H_

#include <string>
#include <stdint.h>
#include <math.h>
#include <cmath>
#include <netdb.h>
#include <sys/stat.h>

#define IS_BIG_ENDIAN (*(uint16_t *)"\0\xff" < 0x100)

namespace utils
{

    /**
     * @brief      Gets uint64_t from msg.
     *
     * @param      msg   The message
     *
     * @return     uint64_t.
     */
    uint64_t getUInt64(unsigned char *msg);
    
    /**
     * @brief      Gets uint32_t from msg.
     *
     * @param      msg   The message
     *
     * @return     uint32_t.
     */
    uint32_t getUInt32(unsigned char *msg);
    
    /**
     * @brief      Gets uint16_t from msg.
     *
     * @param      msg   The message
     *
     * @return     uint16_t.
     */
    uint16_t getUInt16(unsigned char *msg);
    
    /**
     * @brief      Gets uint8_t from msg.
     *
     * @param      msg   The message
     *
     * @return     uint8_t.
     */
    uint8_t  getUInt8(char *msg);
    
    /**
     * @brief      Gets value in meters from msg.
     *
     * @param      msg   The message
     *
     * @return     Value in meters.
     */
    double   getMeter(unsigned char *msg);
    
    /**
     * @brief      Gets value in radians from msg with uint32_t.
     *
     * @param      msg   The message
     *
     * @return     Value in radians.
     */
    double   getRad32(unsigned char *msg);
        
    /**
     * @brief      Gets value in radians from msg with uint16_t.
     *
     * @param      msg   The message
     *
     * @return     Value in radians.
     */
    double   getRad16(unsigned char *msg);

    /**
     * @brief      Convert angle in degrees to radians
     *
     * @param[in]  degree  The input angle in degrees
     *
     * @return     Output angle in radians
     */
    double   deg2rad(double degree);
    
    /**
     * @brief      Convert angle in radians to degrees
     *
     * @param[in]  degree  The input angle in radians
     *
     * @return     Output angle in degrees
     */
    double   rad2deg(double radians);

    /**
     * @brief      Gets timestamp value in seconds from msg.
     *
     * @param      msg   The message
     *
     * @return     Timestamp value in seconds.
     */
    double   getTimestampInSecs(unsigned char *msg);

    void     rotate2d(double& x, double& y, double th);

    void     rotate2d(float& x, float& y, float th);

    /**
     * @brief      Compute angular difference between two inputs in the range -pi, +pi
     *
     * @param[in]      currTh   Frist input angle
     * @param[in]      prevTh   Second input angle
     *
     * @return     Angular difference between currTh and prevTh in radians.
     */
    double getAngularDiff(const double& currTh, const double& prevTh);

    double getAngularSum(const double& currTh, const double& prevTh);

    void correctRadRange(float &th);

    void correctRadRange(double &th);

    /**
     * @brief      Checks if a file exists or not
     *
     * @param[in]  filename  The filename
     *
     * @return     True if file with path filename exists, false otherwise
     */
    bool fileExists(const std::string& filename);

};

#endif