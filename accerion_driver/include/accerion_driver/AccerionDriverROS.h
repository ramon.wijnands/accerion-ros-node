/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef ACCERIONDRIVERROS_H
#define ACCERIONDRIVERROS_H

#include <netinet/in.h>
#include <bitset>
#include <fstream>
#include <ros/ros.h>

#include <std_msgs/Bool.h>
#include <nav_msgs/Odometry.h>
#include <nav_msgs/OccupancyGrid.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud2_iterator.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/PolygonStamped.h>
#include <geometry_msgs/Point32.h>
#include <visualization_msgs/MarkerArray.h>

#include <tf/tf.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>
#include <tf2_ros/static_transform_broadcaster.h>

#include <accerion_driver_msgs/SensorDiagnostics.h>
#include <accerion_driver_msgs/DriftCorrectionDetails.h>
#include <accerion_driver_msgs/SetRecoveryMode.h>
#include <accerion_driver_msgs/Cluster.h>
#include <accerion_driver_msgs/ModeCommand.h>
#include <accerion_driver_msgs/ModeClusterCommand.h>
#include <accerion_driver_msgs/RequestCommand.h>
#include <accerion_driver_msgs/ParentChildFrames.h>
#include <accerion_driver_msgs/Pose.h>
#include <accerion_driver_msgs/UDPSettings.h>
#include <accerion_driver_msgs/Recordings.h>
#include <accerion_driver_msgs/FileTransfer.h>
#include <accerion_driver_msgs/MapTransfer.h>
#include <accerion_driver_msgs/Logs.h>
#include <accerion_driver_msgs/SetIP.h>
#include <accerion_driver_msgs/SecondaryLF.h>
#include <accerion_driver_msgs/DateTime.h>
#include <std_srvs/SetBool.h>
#include <std_srvs/Empty.h>

#include <arpa/inet.h>

#include "AccerionSensorAPI.h"
#include "utils.h"

class AccerionDriverROS
{
public:
    AccerionDriverROS();

    int run();
    int shutdown();

    uint32_t sensorSerial_;

    std::string sensorIp_;

    std::string connectionType_;

private:
    void correctedPoseAck(CorrectedPose cp);

    void uncorrectedPoseAck(UncorrectedPose cp);

    void diagnosticsAck(Diagnostics diag);

    void driftCorrectionAck(DriftCorrection dc);

    void lineFollowerAck(LineFollowerData lfd);

    void signatureMarkerAck(MarkerPosPacket mpp);

    void signatureMarkerStartStopAck(Acknowledgement ack);

    void arucoMarkerAck(ArucoMarker am);

    void positionSetAck(Pose sp);

    void consoleOutputAck(std::string output);

    void clusterRemovedAck(uint16_t clusterID);

    void lineFollowerModeAck(Acknowledgement ack);

    void recoveryModeAck(Acknowledgement ack);

    void clusterLibraryRemovedAck(Acknowledgement ack);

    void idleModeAck(Acknowledgement ack);

    void localizationModeAck(Acknowledgement ack);

    void mappingModeAck(Acknowledgement ack);

    void softwareVersionAck(SoftwareVersion sv);

    void softwareDetailsAck(SoftwareDetails sd);

    void mountPoseAck(Pose mp);

    void ipAddressAck(IPAddressExtended ip);

    void callbackResetPose(const geometry_msgs::PoseStamped::ConstPtr& resetPose);

    void callbackExternalReferencePose(const geometry_msgs::PoseWithCovarianceStampedPtr& ext_ref_msg);

    bool callbackServiceRecoveryMode(accerion_driver_msgs::SetRecoveryMode::Request  &req,
                                           accerion_driver_msgs::SetRecoveryMode::Response &res);
    bool callbackServiceDeleteCluster(accerion_driver_msgs::Cluster::Request  &req,
                                      accerion_driver_msgs::Cluster::Response &res);
    bool callbackServiceDeleteAllClusters(std_srvs::SetBool::Request  &req,
                                          std_srvs::SetBool::Response &res);
    bool callbackServiceMode(accerion_driver_msgs::ModeCommand::Request  &req,
                             accerion_driver_msgs::ModeCommand::Response &res);
    bool callbackServiceModeWithCluster(accerion_driver_msgs::ModeClusterCommand::Request  &req,
                                        accerion_driver_msgs::ModeClusterCommand::Response &res);
    bool callbackServiceRequest(accerion_driver_msgs::RequestCommand::Request  &req,
                                accerion_driver_msgs::RequestCommand::Response &res);
    bool callbackServiceResetPoseByTfLookup(accerion_driver_msgs::ParentChildFrames::Request  &req,
                                            accerion_driver_msgs::ParentChildFrames::Response &res);
    bool callbackServiceSetPose(accerion_driver_msgs::Pose::Request  &req,
                                accerion_driver_msgs::Pose::Response &res);
    bool callbackServiceSetInternalTracking(std_srvs::SetBool::Request  &req,
                                          std_srvs::SetBool::Response &res);
    bool callbackServiceSetUDPSettings(accerion_driver_msgs::UDPSettings::Request  &req,
                                       accerion_driver_msgs::UDPSettings::Response &res);
    bool callbackServiceRecordings(accerion_driver_msgs::Recordings::Request &req,
                                    accerion_driver_msgs::Recordings::Response &res);
    bool callbackServiceUpdate(accerion_driver_msgs::FileTransfer::Request &req,
                                    accerion_driver_msgs::FileTransfer::Response &res);
    bool callbackServiceLogs(accerion_driver_msgs::Logs::Request &req,
                                    accerion_driver_msgs::Logs::Response &res);
    bool callbackServiceGetMap(accerion_driver_msgs::FileTransfer::Request &req,
                                    accerion_driver_msgs::FileTransfer::Response &res);
    bool callbackServiceSendMap(accerion_driver_msgs::MapTransfer::Request &req,
                                    accerion_driver_msgs::MapTransfer::Response &res);
    bool callbackServiceGetG2O(accerion_driver_msgs::FileTransfer::Request &req,
                                    accerion_driver_msgs::FileTransfer::Response &res);
    bool callbackServiceSendG2O(accerion_driver_msgs::FileTransfer::Request &req,
                                    accerion_driver_msgs::FileTransfer::Response &res);
    bool callbackServiceImportG2O(std_srvs::Empty::Request& request, 
                                    std_srvs::Empty::Response& response);
    bool callbackServiceSearchLoopClosures(std_srvs::SetBool::Request  &req,
                                          std_srvs::SetBool::Response &res);
    bool callbackServiceSetIPAddress(accerion_driver_msgs::SetIP::Request  &req,
                                          accerion_driver_msgs::SetIP::Response &res);
    bool callbackServiceSecondaryLinefollower(accerion_driver_msgs::SecondaryLF::Request  &req,
                                          accerion_driver_msgs::SecondaryLF::Response &res);
    bool callbackServiceSetDateTime(accerion_driver_msgs::DateTime::Request  &req,
                                          accerion_driver_msgs::DateTime::Response &res);

    double getThetaFromQuat(const geometry_msgs::Quaternion quatIn);
    
    bool debugModeStreaming_;                            

    /*Comm related*/
    AccerionSensorManager * sensorManager_ = nullptr;
    AccerionUpdateServiceManager * updateManager_ = nullptr;
    AccerionSensor * accerionSensor_ = nullptr;
    AccerionUpdateService * updateService_ = nullptr;

    ros::NodeHandle nh_; 
    ros::NodeHandle* pnh_;
    std::string nodeNs_;

    ros::Publisher  pubAbsOdom_, pubRelOdom_, pubAbsCorrections_;
    ros::Publisher  pubDriftDetails_;
    ros::Publisher  pubDiagnostics_;
    ros::Publisher  pubMapAsPC2_;
    ros::Publisher  pubLineFollower_;
    ros::Publisher  pubArucoVisualization_;
    ros::Subscriber subsResetPose_;
    ros::Subscriber subsExternalOdometry_;

    ros::ServiceServer srvServerMode_;
    ros::ServiceServer srvServerModeWithCluster_;
    ros::ServiceServer srvServerRequest_;

    ros::ServiceServer serviceServerRecoveryMode_;
    ros::ServiceServer serviceServerDeleteCluster_;
    ros::ServiceServer serviceServerDeleteAllClusters_;
    ros::ServiceServer serviceServerResetPoseByTfLookup_;      
    ros::ServiceServer serviceServerSetPose_;
    ros::ServiceServer serviceServerSetInternalTracking_;
    ros::ServiceServer serviceServerSetUDPSettings_;

    ros::ServiceServer serviceServerRecordings_;
    ros::ServiceServer serviceServerUpdate_;
    ros::ServiceServer serviceServerLogs_;
    ros::ServiceServer serviceServerGetMap_;
    ros::ServiceServer serviceServerSendMap_;
    ros::ServiceServer serviceServerGetG2O_;
    ros::ServiceServer serviceServerSendG2O_;
    ros::ServiceServer serviceServerImportG2O_;
    ros::ServiceServer serviceServerSearchLoopClosures_;
    ros::ServiceServer serviceServerSetIP_;
    ros::ServiceServer serviceServerSecondaryLF_;
    ros::ServiceServer serviceServerSetDateTime_;

    tf::TransformBroadcaster                 tfBroadcaster_;
    tf2_ros::StaticTransformBroadcaster      staticTfBroadcaster_; 
       
    geometry_msgs::TransformStamped  relOdomTrans_, absPoseTrans_;
    geometry_msgs::PoseWithCovarianceStamped ext_ref_msg_;

    std::string   absRefFrame_, relRefFrame_, absBaseFrame_, relBaseFrame_, relSensorFrame_, absSensorFrame_;
    std::string   extRefPoseTopic_;

    double extRefPoseLatency_; // fixed latency options in ms, if unset (default is 0), latency will be calculated from ros timestamps

    bool relOdomOn_, relTransOn_;
    bool absOdomOn_, absTransOn_; 
    bool driftDetailsOn_; 
    bool relInAbsTransOn_;

    int           correctionCounter_;

    tf::TransformListener tfListener_;   

    sensor_msgs::PointCloud2            mapPointCloud_;
    sensor_msgs::PointCloud2Modifier*   pc2Modifier_;

    double rawTimestampAbs_, rawTimestampRel_;

};

#endif // ACCERIONDRIVERROS_H
