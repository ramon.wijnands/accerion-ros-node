/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "utils.h"

uint64_t utils::getUInt64(unsigned char *msg)
{   
    uint64_t a = *((uint64_t*)msg);

    a = be64toh(a);    // network is big endian
    
    return a;
}

uint32_t utils::getUInt32(unsigned char *msg)
{   
    uint32_t a = *((uint32_t*)msg);
    a = ntohl(a);    
    return a;
}

uint16_t utils::getUInt16(unsigned char *msg)
{
    uint16_t a = *((uint16_t*)msg);
    a = ntohs(a);
    return a;
}

double utils::getMeter(unsigned char *msg)
{
    return ((int32_t) getUInt32(msg)) / 1000000.0;
}

double utils::getRad32(unsigned char *msg)
{
    return deg2rad((int32_t)getUInt32(msg) / 100.0);
}

double utils::getRad16(unsigned char *msg)
{
    return deg2rad((int16_t)getUInt16(msg) / 100.0);
}

double utils::deg2rad(double degree)
{
    return (degree / 180.0) * M_PI; 
}
double utils::rad2deg(double radians)
{
    return (radians * 180.0) / M_PI; 
}

double utils::getTimestampInSecs(unsigned char *msg)
{
    return ((double)getUInt64(msg)) / 1000000.0;
}

void utils::rotate2d(double& x, double& y, double th)
{
    double x0 = x;
    double y0 = y;

    x = cos(th)*x0 - sin(th)*y0;
    y = sin(th)*x0 + cos(th)*y0;
}

void utils::rotate2d(float& x, float& y, float th)
{
    float x0 = x;
    float y0 = y;

    x = cos(th)*x0 - sin(th)*y0;
    y = sin(th)*x0 + cos(th)*y0;
}

double utils::getAngularDiff(const double& currTh, const double& prevTh)
{   // returns angular difference between two inputs in the range -pi, +pi

    double angDiff=0.0;

    if(currTh*prevTh<0 && std::fabs(currTh-prevTh)>M_PI )
    {
        if(currTh>0)
        {
            return currTh - prevTh - 2.0*M_PI;
        }
        else
        {
            return currTh - prevTh + 2.0*M_PI ;
        }
    }
    else
    {
        return currTh-prevTh;
    }
}

double utils::getAngularSum(const double& currTh, const double& prevTh)
{   // returns angular sum between two inputs in the range -pi, +pi

    double result=0.0;

    if(currTh*prevTh<0 && std::fabs(currTh-prevTh)>M_PI )
    {
        if(currTh>0)
        {
            result = currTh + prevTh - 2.0*M_PI;
            correctRadRange(result);
            return result; //currTh + prevTh - 2.0*M_PI;
        }
        else
        {
            result = currTh + prevTh + 2.0*M_PI ;
            correctRadRange(result);
            return result; //currTh + prevTh + 2.0*M_PI ;
        }
    }
    else
    {
        result = currTh + prevTh; 
        correctRadRange(result);
        return result; //currTh+prevTh;
    }
}

void utils::correctRadRange(float &th)
{
    if(th<-M_PI) th += 2.0f*M_PI;
    if(th>M_PI) th += -2.0f*M_PI;
}

void utils::correctRadRange(double &th)
{
    if(th<-M_PI) th += 2.0*M_PI;
    if(th>M_PI) th += -2.0*M_PI;
}

bool utils::fileExists(const std::string& filename)
{
    struct stat buf;
    if (stat(filename.c_str(), &buf) != -1)
    {
        return true;
    }
    return false;
}
