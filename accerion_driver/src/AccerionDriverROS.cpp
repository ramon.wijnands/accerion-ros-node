/* Copyright (c) 2017-2019, Accerion (Unconstrained Robotics B.V.)
 * All rights reserved.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "AccerionDriverROS.h"

AccerionDriverROS::AccerionDriverROS()
{
    pnh_ = new ros::NodeHandle("~");

    nodeNs_ = ros::this_node::getName();
    ROS_INFO("nodeNs_: %s", nodeNs_.c_str());
    // read ros params
    absRefFrame_ = pnh_->param<std::string>("abs_ref_frame", "map");

    sensorSerial_ = pnh_->param("serial_number", 0);
    connectionType_ = pnh_->param<std::string>("connection_type", "CONNECTION_TCP");
    relOdomOn_ = pnh_->param("ros_publishing/rel_odom", true);
    relTransOn_ = pnh_->param("ros_publishing/rel_tf", true);
    absOdomOn_ = pnh_->param("ros_publishing/abs_odom", true);
    absTransOn_ = pnh_->param("ros_publishing/abs_tf", true);
    relInAbsTransOn_ = pnh_->param("ros_publishing/rel_in_abs_tf", true);
    absBaseFrame_ = pnh_->param<std::string>("abs_base_frame", "sensor_output_abs");
    relRefFrame_ = pnh_->param<std::string>("rel_ref_frame", "odom");
    relBaseFrame_ = pnh_->param<std::string>("rel_base_frame", "sensor_output_rel");
    relSensorFrame_ = pnh_->param<std::string>("rel_sensor_frame", "sensor_mount_rel");
    absSensorFrame_ = pnh_->param<std::string>("abs_sensor_frame", "sensor_mount_abs");
    debugModeStreaming_ = pnh_->param("debug/debug_sensor_messages", false);
    extRefPoseTopic_ = pnh_->param<std::string>("external_reference_pose_topic", "ext_ref");
    extRefPoseLatency_ = pnh_->param("external_reference_pose_latency", 0.0);

    if (pnh_->param("use_external_reference_pose", false))
    {
        subsExternalOdometry_ = nh_.subscribe(extRefPoseTopic_, 1, &AccerionDriverROS::callbackExternalReferencePose, this, ros::TransportHints().tcpNoDelay());
        ROS_INFO("NOTE: External odometry input %s will be used to set sensor pose continuously!!! EXPERIMENTAL", extRefPoseTopic_.c_str());
    }

    // initialize publishers and subscribers
    if (relOdomOn_)
        pubRelOdom_ = nh_.advertise<nav_msgs::Odometry>("rel_odom", 1);
    if (absOdomOn_)
        pubAbsOdom_ = nh_.advertise<nav_msgs::Odometry>("abs_map", 1);

    pubDriftDetails_ = nh_.advertise<accerion_driver_msgs::DriftCorrectionDetails>("correction_details", 1);
    pubDiagnostics_ = nh_.advertise<accerion_driver_msgs::SensorDiagnostics>("diagnostics", 1);
    pubLineFollower_ = nh_.advertise<geometry_msgs::PolygonStamped>("line_follower", 1);
    pubAbsCorrections_ = nh_.advertise<nav_msgs::Odometry>("corrections", 1);
    pubMapAsPC2_ = nh_.advertise<sensor_msgs::PointCloud2>("map", 10, true);
    pubArucoVisualization_ = nh_.advertise<visualization_msgs::MarkerArray>("aruco_in_sensor_frame", 5, false);

    subsResetPose_ = nh_.subscribe("reset_pose_manual", 1, &AccerionDriverROS::callbackResetPose, this);

    serviceServerRecoveryMode_ = nh_.advertiseService("mode/set_recovery_search_radius", &AccerionDriverROS::callbackServiceRecoveryMode, this);
    serviceServerDeleteCluster_ = nh_.advertiseService("delete_cluster", &AccerionDriverROS::callbackServiceDeleteCluster, this);
    serviceServerDeleteAllClusters_ = nh_.advertiseService("delete_all_clusters", &AccerionDriverROS::callbackServiceDeleteAllClusters, this);
    srvServerMode_ = nh_.advertiseService("mode", &AccerionDriverROS::callbackServiceMode, this);
    srvServerModeWithCluster_ = nh_.advertiseService("mode_with_cluster", &AccerionDriverROS::callbackServiceModeWithCluster, this);
    srvServerRequest_ = nh_.advertiseService("request", &AccerionDriverROS::callbackServiceRequest, this);
    serviceServerResetPoseByTfLookup_ = nh_.advertiseService("reset_pose_by_tf_lookup", &AccerionDriverROS::callbackServiceResetPoseByTfLookup, this);
    serviceServerSetPose_ = nh_.advertiseService("set_pose", &AccerionDriverROS::callbackServiceSetPose, this);
    serviceServerSetInternalTracking_ = nh_.advertiseService("set_internal_tracking", &AccerionDriverROS::callbackServiceSetInternalTracking, this);
    serviceServerSetUDPSettings_ = nh_.advertiseService("set_udp_settings", &AccerionDriverROS::callbackServiceSetUDPSettings, this);

    serviceServerRecordings_ = nh_.advertiseService("recordings", &AccerionDriverROS::callbackServiceRecordings, this);
    serviceServerUpdate_     = nh_.advertiseService("update", &AccerionDriverROS::callbackServiceUpdate, this);
    serviceServerLogs_       = nh_.advertiseService("logs", &AccerionDriverROS::callbackServiceLogs, this);
    serviceServerGetMap_     = nh_.advertiseService("get_map", &AccerionDriverROS::callbackServiceGetMap, this);
    serviceServerSendMap_    = nh_.advertiseService("send_map", &AccerionDriverROS::callbackServiceSendMap, this);
    serviceServerGetG2O_     = nh_.advertiseService("get_g2o", &AccerionDriverROS::callbackServiceGetG2O, this);
    serviceServerSendG2O_    = nh_.advertiseService("send_g2o", &AccerionDriverROS::callbackServiceSendG2O, this);
    serviceServerImportG2O_  = nh_.advertiseService("import_g2o", &AccerionDriverROS::callbackServiceImportG2O, this);
    serviceServerSearchLoopClosures_    = nh_.advertiseService("search_loop_closures", &AccerionDriverROS::callbackServiceSearchLoopClosures, this);
    serviceServerSetIP_      = nh_.advertiseService("set_ip", &AccerionDriverROS::callbackServiceSetIPAddress, this);
    serviceServerSecondaryLF_ = nh_.advertiseService("secondary_linefollower", &AccerionDriverROS::callbackServiceSecondaryLinefollower, this);
    serviceServerSetDateTime_ = nh_.advertiseService("set_datetime", &AccerionDriverROS::callbackServiceSetDateTime, this);

    //init comms
    sensorManager_ = AccerionSensorManager::getInstance();
    updateManager_ = AccerionUpdateServiceManager::getInstance();

    //initialize other variables
    correctionCounter_ = 0;
    sensorIp_ = "0";

    mapPointCloud_.header.frame_id = absRefFrame_;
    mapPointCloud_.is_bigendian = false;
    mapPointCloud_.is_dense = true;

    mapPointCloud_.width = 0;
    mapPointCloud_.height = 1;
    mapPointCloud_.fields.resize(3);

    // Set x/y/z as the only fields
    mapPointCloud_.fields[0].name = "x";
    mapPointCloud_.fields[1].name = "y";
    mapPointCloud_.fields[2].name = "z";

    int offset = 0;
    // All offsets are *4, as all field data types are float32
    for (size_t d = 0; d < mapPointCloud_.fields.size(); ++d, offset += 4)
    {
        mapPointCloud_.fields[d].count = 1;
        mapPointCloud_.fields[d].offset = offset;
        mapPointCloud_.fields[d].datatype = sensor_msgs::PointField::FLOAT32;
    }

    mapPointCloud_.point_step = offset;
    mapPointCloud_.row_step = mapPointCloud_.point_step * mapPointCloud_.width;

    mapPointCloud_.data.resize(mapPointCloud_.width * mapPointCloud_.point_step);

    pc2Modifier_ = new sensor_msgs::PointCloud2Modifier(mapPointCloud_);

    pc2Modifier_->setPointCloud2Fields(6, "x", 1, sensor_msgs::PointField::FLOAT32,
                                       "y", 1, sensor_msgs::PointField::FLOAT32,
                                       "z", 1, sensor_msgs::PointField::FLOAT32,
                                       "markerID", 1, sensor_msgs::PointField::UINT32,
                                       "clusterID", 1, sensor_msgs::PointField::UINT16,
                                       "markerStatus", 1, sensor_msgs::PointField::UINT8);
}

int AccerionDriverROS::run()
{
    ROS_INFO("%s: Starting Accerion Ros Node..", nodeNs_.c_str());
    sleep(5);
    double x, y, th;
    geometry_msgs::Quaternion q;

    Address localIP;
    ConnectionType selectedConnectionType = ConnectionType::CONNECTION_TCP;
    if (connectionType_ == "CONNECTION_UDP_BROADCAST")
    {
        selectedConnectionType = ConnectionType::CONNECTION_UDP_BROADCAST;
    }
    if (connectionType_ == "CONNECTION_UDP_UNICAST")
    {
        selectedConnectionType = ConnectionType::CONNECTION_UDP_UNICAST;
    }
    if (connectionType_ == "CONNECTION_TCP_DISABLE_UDP")
    {
        selectedConnectionType = ConnectionType::CONNECTION_TCP_DISABLE_UDP;
    }
    bool sensorConnected = false;
    bool updateServiceConnected = false;
    bool fixedSerial = true;
    if(sensorSerial_ == 0)
    {
        fixedSerial = false;
        ROS_WARN("%s: No serial given, will connect to the first detected serial number..", nodeNs_.c_str());
    }

    while (sensorConnected == false || updateServiceConnected == false) //todo stop mechanism
    {
        ROS_INFO("%s: Trying to connect to sensor with SN: %d..", nodeNs_.c_str(), sensorSerial_);
        if(fixedSerial)
        {
            if(!sensorConnected)
                accerionSensor_ = sensorManager_->getAccerionSensorBySerial(std::to_string(sensorSerial_), localIP, selectedConnectionType);
            if(!updateServiceConnected)
                updateService_ = updateManager_->getAccerionUpdateServiceBySerial(std::to_string(sensorSerial_), localIP);
            if(accerionSensor_ != nullptr)
                sensorConnected = true;
            if(updateService_ != nullptr)
                updateServiceConnected = true;
        }
        else
        {
            std::vector<SensorDetails> sensors = sensorManager_->getDetectedSensors();
            if (sensors.size() == 0)
            {
                ROS_WARN("%s: no sensors found..", nodeNs_.c_str());
                sleep(2);
            }
            else
            {
                sensorSerial_ = static_cast<uint32_t>(std::stoul(sensors[0].serialNumber));
                ROS_WARN("%s: no serial set, using first found accerion sensor with SN: %d", nodeNs_.c_str(), sensorSerial_);
                fixedSerial = true;
            }   
        }  
    }
    ROS_INFO("%s: Subscribing to API topics..", nodeNs_.c_str());
    // connect everything
    auto correctedPoseCB = std::bind(&AccerionDriverROS::correctedPoseAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToCorrectedPose(correctedPoseCB);
    auto uncorrectedPoseCB = std::bind(&AccerionDriverROS::uncorrectedPoseAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToUncorrectedPose(uncorrectedPoseCB);
    auto diagCB = std::bind(&AccerionDriverROS::diagnosticsAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToDiagnostics(diagCB);
    auto driftCorrectionCB = std::bind(&AccerionDriverROS::driftCorrectionAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToDriftCorrections(driftCorrectionCB);
    auto lineFollowerCB = std::bind(&AccerionDriverROS::lineFollowerAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToLineFollowerData(lineFollowerCB);
    auto sigMarkerDataCB = std::bind(&AccerionDriverROS::signatureMarkerAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToMarkerPosPacket(sigMarkerDataCB);
    auto sigMarkerStartStopCB = std::bind(&AccerionDriverROS::signatureMarkerStartStopAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToMarkerPosPacketStartStop(sigMarkerStartStopCB);
    auto arucoMarkerCB = std::bind(&AccerionDriverROS::arucoMarkerAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToArucoMarkers(arucoMarkerCB);
    auto positionSetCB = std::bind(&AccerionDriverROS::positionSetAck, this, std::placeholders::_1);
    accerionSensor_->setSensorPoseCallback(positionSetCB);
    auto consoleOutputCB = std::bind(&AccerionDriverROS::consoleOutputAck, this, std::placeholders::_1);
    accerionSensor_->subscribeToConsoleOutputInfo(consoleOutputCB);
    auto clusterRemovedCB = std::bind(&AccerionDriverROS::clusterRemovedAck, this, std::placeholders::_1);
    accerionSensor_->setRemoveClusterFromLibraryCallback(clusterRemovedCB);
    auto lineFollowerModeCB = std::bind(&AccerionDriverROS::lineFollowerModeAck, this, std::placeholders::_1);
    accerionSensor_->setLineFollowingCallback(lineFollowerModeCB);
    auto recoveryModeCB = std::bind(&AccerionDriverROS::recoveryModeAck, this, std::placeholders::_1);
    accerionSensor_->setRecoveryModeCallback(recoveryModeCB);
    auto clusterLibRemovedCB = std::bind(&AccerionDriverROS::clusterLibraryRemovedAck, this, std::placeholders::_1);
    accerionSensor_->setClearClusterLibraryCallback(clusterLibRemovedCB);
    auto idleModeCB = std::bind(&AccerionDriverROS::idleModeAck, this, std::placeholders::_1);
    accerionSensor_->setIdleModeCallback(idleModeCB);
    auto localizationModeCB = std::bind(&AccerionDriverROS::localizationModeAck, this, std::placeholders::_1);
    accerionSensor_->setAbsoluteModeCallback(localizationModeCB);
    auto mappingModeCB = std::bind(&AccerionDriverROS::mappingModeAck, this, std::placeholders::_1);
    accerionSensor_->setMappingCallback(mappingModeCB);
    auto swVersionCB = std::bind(&AccerionDriverROS::softwareVersionAck, this, std::placeholders::_1);
    accerionSensor_->setSoftwareVersionCallback(swVersionCB);
    auto swDetailsCB = std::bind(&AccerionDriverROS::softwareDetailsAck, this, std::placeholders::_1);
    accerionSensor_->setSoftwareDetailsCallback(swDetailsCB);
    auto mountPoseCB = std::bind(&AccerionDriverROS::mountPoseAck, this, std::placeholders::_1);
    accerionSensor_->setSensorMountPoseCallback(mountPoseCB);
    auto ipAddrCB = std::bind(&AccerionDriverROS::ipAddressAck, this, std::placeholders::_1);
    accerionSensor_->setGetIPAddressCallback(ipAddrCB);

    return 0;
}

int AccerionDriverROS::shutdown()
{
    //delete udpManager_;
}

void AccerionDriverROS::correctedPoseAck(CorrectedPose cp)
{
    rawTimestampAbs_ = cp.timeStamp;
    ros::Time rosTimeStamp = ros::Time::now();
    if (absOdomOn_)
    {
        nav_msgs::Odometry absPoseMsg;
        absPoseMsg.header.stamp = rosTimeStamp;
        absPoseMsg.header.frame_id = absRefFrame_;
        absPoseMsg.child_frame_id = absBaseFrame_;
        absPoseMsg.pose.pose.position.x = cp.pose.x;
        absPoseMsg.pose.pose.position.y = cp.pose.y;
        absPoseMsg.pose.pose.position.z = 0.0;
        double theta = utils::deg2rad(cp.pose.heading);
        absPoseMsg.pose.pose.orientation.x = 0.0;
        absPoseMsg.pose.pose.orientation.y = 0.0;
        absPoseMsg.pose.pose.orientation.z = sin(theta / 2.0);
        absPoseMsg.pose.pose.orientation.w = cos(theta / 2.0);
        absPoseMsg.twist.twist.linear.x = cp.xVel;
        absPoseMsg.twist.twist.linear.y = cp.yVel;
        absPoseMsg.twist.twist.angular.z = cp.thVel;

        uint64_t poseStdDevX = cp.standardDeviation.x;
        uint64_t poseStdDevY = cp.standardDeviation.y;
        uint64_t poseStdDevTh = cp.standardDeviation.theta;
        uint64_t velStdDevX = cp.standardDeviationVelocity.x;
        uint64_t velStdDevY = cp.standardDeviationVelocity.y;
        uint64_t velStdDevTh = cp.standardDeviationVelocity.theta;

        absPoseMsg.pose.covariance[0] = (poseStdDevX * poseStdDevX) / 1000000000000.0;
        absPoseMsg.pose.covariance[7] = (poseStdDevY * poseStdDevY) / 1000000000000.0;
        absPoseMsg.pose.covariance[35] = (poseStdDevTh * poseStdDevTh) * (M_PI * M_PI) / (180.0 * 180.0 * 10000.0);

        absPoseMsg.twist.covariance[0] = velStdDevX * velStdDevX / 1000000000000.0;
        absPoseMsg.twist.covariance[7] = velStdDevY * velStdDevY / 1000000000000.0;
        absPoseMsg.twist.covariance[35] = (velStdDevTh * velStdDevTh) * (M_PI * M_PI) / (180.0 * 180.0 * 10000.0);
        pubAbsOdom_.publish(absPoseMsg);
    }

    absPoseTrans_.header.stamp = rosTimeStamp;
    absPoseTrans_.header.frame_id = absRefFrame_;
    absPoseTrans_.child_frame_id = absBaseFrame_;
    absPoseTrans_.transform.translation.x = cp.pose.x;
    absPoseTrans_.transform.translation.y = cp.pose.y;
    absPoseTrans_.transform.translation.z = 0.0;
    double theta = utils::deg2rad(cp.pose.heading);
    absPoseTrans_.transform.rotation.x = 0.0;
    absPoseTrans_.transform.rotation.y = 0.0;
    absPoseTrans_.transform.rotation.z = sin(theta / 2.0);
    absPoseTrans_.transform.rotation.w = cos(theta / 2.0);

    if (absTransOn_)
    {
        tfBroadcaster_.sendTransform(absPoseTrans_);
    }

    if (relInAbsTransOn_ && rawTimestampAbs_ == rawTimestampRel_)
    {
        double xtmp, ytmp, thtmp;
        xtmp = -relOdomTrans_.transform.translation.x;
        ytmp = -relOdomTrans_.transform.translation.y;
        thtmp = getThetaFromQuat(relOdomTrans_.transform.rotation);

        utils::rotate2d(xtmp, ytmp, -thtmp);
        utils::rotate2d(xtmp, ytmp, theta);

        double relInAbsTh = utils::getAngularDiff(theta, thtmp);

        geometry_msgs::TransformStamped relInAbsTrans;
        relInAbsTrans.header.stamp = rosTimeStamp;
        relInAbsTrans.header.frame_id = absRefFrame_;
        relInAbsTrans.child_frame_id = relRefFrame_;
        relInAbsTrans.transform.translation.x = cp.pose.x + xtmp;
        relInAbsTrans.transform.translation.y = cp.pose.y + ytmp;
        relInAbsTrans.transform.translation.z = 0.0;
        relInAbsTrans.transform.rotation.x = 0.0;
        relInAbsTrans.transform.rotation.y = 0.0;
        relInAbsTrans.transform.rotation.z = sin(relInAbsTh / 2.0);
        relInAbsTrans.transform.rotation.w = cos(relInAbsTh / 2.0);

        tfBroadcaster_.sendTransform(relInAbsTrans);
    }
}

void AccerionDriverROS::uncorrectedPoseAck(UncorrectedPose cp)
{
    ros::Time rosTimeStamp = ros::Time::now();
    rawTimestampRel_ = cp.timeStamp;
    if (relOdomOn_)
    {
        nav_msgs::Odometry relOdomMsg;
        relOdomMsg.header.stamp = rosTimeStamp;
        relOdomMsg.header.frame_id = relRefFrame_;
        relOdomMsg.child_frame_id = relBaseFrame_;

        relOdomMsg.pose.pose.position.x = cp.pose.x;
        relOdomMsg.pose.pose.position.y = cp.pose.y;
        relOdomMsg.pose.pose.position.z = 0.0;

        double theta = utils::deg2rad(cp.pose.heading);
        relOdomMsg.pose.pose.orientation.x = 0.0;
        relOdomMsg.pose.pose.orientation.y = 0.0;
        relOdomMsg.pose.pose.orientation.z = sin(theta / 2.0);
        relOdomMsg.pose.pose.orientation.w = cos(theta / 2.0);

        relOdomMsg.twist.twist.linear.x = cp.xVel;
        relOdomMsg.twist.twist.linear.y = cp.yVel;
        relOdomMsg.twist.twist.angular.z = cp.thVel;

        uint64_t velStdDevX = cp.standardDeviationVelocity.x;
        uint64_t velStdDevY = cp.standardDeviationVelocity.y;
        uint64_t velStdDevTh = cp.standardDeviationVelocity.theta;

        relOdomMsg.twist.covariance[0] = (velStdDevX * velStdDevX) / 1000000000000.0;
        relOdomMsg.twist.covariance[7] = (velStdDevY * velStdDevY) / 1000000000000.0;
        relOdomMsg.twist.covariance[35] = (velStdDevTh * velStdDevTh * M_PI * M_PI) / (180.0 * 180.0 * 10000.0);
        pubRelOdom_.publish(relOdomMsg);
    }

    double theta = utils::deg2rad(cp.pose.heading);
    relOdomTrans_.header.stamp = rosTimeStamp;
    relOdomTrans_.header.frame_id = relRefFrame_;
    relOdomTrans_.child_frame_id = relBaseFrame_;
    relOdomTrans_.transform.translation.x = cp.pose.x;
    relOdomTrans_.transform.translation.y = cp.pose.y;
    relOdomTrans_.transform.translation.z = 0.0;
    relOdomTrans_.transform.rotation.x = 0.0;
    relOdomTrans_.transform.rotation.y = 0.0;
    relOdomTrans_.transform.rotation.z = sin(theta / 2.0);
    relOdomTrans_.transform.rotation.w = cos(theta / 2.0);

    if (relTransOn_)
    {
        tfBroadcaster_.sendTransform(relOdomTrans_);
    }

    if (relInAbsTransOn_ && rawTimestampRel_ == rawTimestampAbs_)
    {
        double xtmp, ytmp, thtmp;
        xtmp = -relOdomTrans_.transform.translation.x;
        ytmp = -relOdomTrans_.transform.translation.y;
        thtmp = getThetaFromQuat(relOdomTrans_.transform.rotation);

        double theta_abs = asin(absPoseTrans_.transform.rotation.z) * 2.0;

        utils::rotate2d(xtmp, ytmp, -thtmp);
        utils::rotate2d(xtmp, ytmp, theta_abs);

        double relInAbsTh = utils::getAngularDiff(theta_abs, thtmp);

        geometry_msgs::TransformStamped relInAbsTrans;
        relInAbsTrans.header.stamp = rosTimeStamp;
        relInAbsTrans.header.frame_id = absRefFrame_;
        relInAbsTrans.child_frame_id = relRefFrame_;
        relInAbsTrans.transform.translation.x = absPoseTrans_.transform.translation.x + xtmp;
        relInAbsTrans.transform.translation.y = absPoseTrans_.transform.translation.y + ytmp;
        relInAbsTrans.transform.translation.z = 0.0;
        relInAbsTrans.transform.rotation.x = 0.0;
        relInAbsTrans.transform.rotation.y = 0.0;
        relInAbsTrans.transform.rotation.z = sin(relInAbsTh / 2.0);
        relInAbsTrans.transform.rotation.w = cos(relInAbsTh / 2.0);

        tfBroadcaster_.sendTransform(relInAbsTrans);
    }
}

void AccerionDriverROS::diagnosticsAck(Diagnostics diag)
{
    ros::Time rosTimeStamp = ros::Time::now();
    accerion_driver_msgs::SensorDiagnostics diagnosticsMsg;

    diagnosticsMsg.header.stamp = rosTimeStamp;
    diagnosticsMsg.serial_number = sensorSerial_;
    diagnosticsMsg.raw_timestamp = diag.timeStamp;

    diagnosticsMsg.modes_code = diag.modes;
    std::bitset<16> modes_bitset(diagnosticsMsg.modes_code);

    diagnosticsMsg.warnings_code = diag.warningCodes;
    std::bitset<16> warnings_bitset(diagnosticsMsg.warnings_code);

    diagnosticsMsg.errors_code = diag.errorCodes;
    std::bitset<32> errors_bitset(diagnosticsMsg.errors_code);

    diagnosticsMsg.status_code = diag.statusCodes;
    std::bitset<8> status_bitset(diagnosticsMsg.status_code);

    for (unsigned int ii = 0; ii < 32; ii++)
    {
        if (ii < 8)
        {
            if (status_bitset.test(ii))
                diagnosticsMsg.active_status += statusMap.find(ii)->second + ",";
        }

        if (ii < 16)
        {
            if (modes_bitset.test(ii))
                diagnosticsMsg.active_modes += modesDescriptionMap.find(ii)->second + ",";

            if (warnings_bitset.test(ii))
                diagnosticsMsg.active_warnings += warningMap.find(ii)->second + ",";
        }

        if (errors_bitset.test(ii))
            diagnosticsMsg.active_errors += errorMap.find(ii)->second + ",";
    }

    pubDiagnostics_.publish(diagnosticsMsg);
}

void AccerionDriverROS::driftCorrectionAck(DriftCorrection dc)
{
    ros::Time rosTimeStamp = ros::Time::now();
    nav_msgs::Odometry correctionsMsg;
    correctionsMsg.header.stamp = rosTimeStamp;

    correctionsMsg.header.frame_id = absRefFrame_;
    correctionsMsg.child_frame_id = absBaseFrame_;

    double x = dc.pose.x;
    double y = dc.pose.y;
    double th = utils::deg2rad(dc.pose.heading);

    correctionsMsg.header.seq = correctionCounter_;
    correctionsMsg.pose.pose.position.x = x;
    correctionsMsg.pose.pose.position.y = y;
    correctionsMsg.pose.pose.position.z = 0.0;

    correctionsMsg.pose.pose.orientation.x = 0.0;
    correctionsMsg.pose.pose.orientation.y = 0.0;
    correctionsMsg.pose.pose.orientation.z = sin(th / 2.0);
    correctionsMsg.pose.pose.orientation.w = cos(th / 2.0);

    /*Note: For now, covariance values in this ROS message are dummy,
                     *since covariance values are not included with the Drift Correction UDP message*/
    correctionsMsg.pose.covariance[0] = 0.0;
    correctionsMsg.pose.covariance[7] = 0.0;
    correctionsMsg.pose.covariance[35] = 0.0;

    correctionCounter_++;

    pubAbsCorrections_.publish(correctionsMsg);

    accerion_driver_msgs::DriftCorrectionDetails driftDetailsMsg;

    driftDetailsMsg.new_corrected_pose = correctionsMsg.pose;

    driftDetailsMsg.correction_x = dc.xDelta;
    driftDetailsMsg.correction_y = dc.yDelta;
    driftDetailsMsg.correction_heading = utils::deg2rad(dc.thDelta);
    driftDetailsMsg.error_heading_deg = dc.thDelta;
    driftDetailsMsg.correct_heading_deg = dc.pose.heading;

    driftDetailsMsg.cumulative_traveled_distance = dc.cumulativeTravelledDistance;
    driftDetailsMsg.cumulative_traveled_rotation = utils::deg2rad(dc.cumulativeTravelledHeading);
    driftDetailsMsg.error_percentage = dc.errorPercentage;
    driftDetailsMsg.cluster_id = dc.QRID;
    driftDetailsMsg.drift_correction_type = dc.typeOfCorrection;
    driftDetailsMsg.acq_quality_estimate = dc.qualityEstimate;

    pubDriftDetails_.publish(driftDetailsMsg);
}

void AccerionDriverROS::lineFollowerAck(LineFollowerData lfd)
{
    ros::Time rosTimeStamp = ros::Time::now();
    geometry_msgs::PolygonStamped lineFollowerArrowPolygon;

    lineFollowerArrowPolygon.header.frame_id = absRefFrame_;
    lineFollowerArrowPolygon.header.stamp = rosTimeStamp;

    lineFollowerArrowPolygon.polygon.points.clear();

    geometry_msgs::Point32 sensorPoint, closestPointOnLine;

    sensorPoint.x = lfd.pose.x;
    sensorPoint.y = lfd.pose.y;
    sensorPoint.z = 0.0;

    closestPointOnLine.x = lfd.closestPointX;
    closestPointOnLine.y = lfd.closestPointY;
    closestPointOnLine.z = 0.0;

    lineFollowerArrowPolygon.polygon.points.push_back(sensorPoint);
    lineFollowerArrowPolygon.polygon.points.push_back(closestPointOnLine);

    pubLineFollower_.publish(lineFollowerArrowPolygon);
}

void AccerionDriverROS::signatureMarkerAck(MarkerPosPacket mpp)
{
    int nMarkerInMessage = mpp.numberOfMarkersInMessage;
    int messageHeaderSize = 7;
    int markerMessageSize = 23;

    int prevMarkerCount = mapPointCloud_.width;
    mapPointCloud_.width += nMarkerInMessage;

    pc2Modifier_->resize(mapPointCloud_.width);

    sensor_msgs::PointCloud2Iterator<float> iterX(mapPointCloud_, "x");
    sensor_msgs::PointCloud2Iterator<float> iterY(mapPointCloud_, "y");
    sensor_msgs::PointCloud2Iterator<float> iterZ(mapPointCloud_, "z");
    sensor_msgs::PointCloud2Iterator<uint16_t> iterClusterID(mapPointCloud_, "clusterID");
    sensor_msgs::PointCloud2Iterator<uint32_t> iterMarkerID(mapPointCloud_, "markerID");
    sensor_msgs::PointCloud2Iterator<uint8_t> iterMarkerStatus(mapPointCloud_, "markerStatus");

    iterX += prevMarkerCount;
    iterY += prevMarkerCount;
    iterZ += prevMarkerCount;
    iterClusterID += prevMarkerCount;
    iterMarkerID += prevMarkerCount;
    iterMarkerStatus += prevMarkerCount;

    for (unsigned int ii = 0; ii < nMarkerInMessage; ii++, ++iterX, ++iterY, ++iterZ, ++iterClusterID, ++iterMarkerID, ++iterMarkerStatus)
    {
        float markerX = mpp.markers[ii].xPos;
        float markerY = mpp.markers[ii].yPos;
        float markerZ = 0.0f;
        uint16_t clusterID = mpp.markers[ii].clusterID;
        uint32_t markerID = mpp.markers[ii].signatureID;
        uint8_t markerStatus = mpp.markers[ii].sigStatus;

        // std::cout   << "marker " << std::setw(5) << ii << ": "
        //             << " x: " << std::setw(10) << std::setprecision(3) << markerX
        //             << " y: " << std::setw(10) << std::setprecision(3) << markerY
        //             << " clID: " << std::setw(10) << std::setprecision(0) << clusterID
        //             << " mID: " << std::setw(10) << std::setprecision(0) << markerID
        //             << " status: " << std::setw(10) << std::setprecision(0) << markerStatus
        //             << std::endl;

        *iterX = markerX;
        *iterY = markerY;
        *iterZ = markerZ;
        *iterClusterID = clusterID;
        *iterMarkerID = markerID;
        *iterMarkerStatus = markerStatus;
    }
}

void AccerionDriverROS::arucoMarkerAck(ArucoMarker am)
{
    visualization_msgs::MarkerArray arucoMarkerArray;
    visualization_msgs::Marker arucoMarker, arucoMarkerText, arucoMarkerArrow;

    // arucoMarker.header.seq = signatureID;
    arucoMarker.header.frame_id = absSensorFrame_;
    arucoMarker.header.stamp = ros::Time::now();
    arucoMarker.id = am.markerID;
    std::stringstream ss;
    ss << arucoMarker.id;
    arucoMarker.ns = ss.str() + "/geometry"; // ss.str();
    arucoMarker.type = visualization_msgs::Marker::CUBE;
    arucoMarker.action = visualization_msgs::Marker::ADD;
    float markerYaw = utils::deg2rad(am.pose.heading);
    arucoMarker.pose.position.x = am.pose.x;
    arucoMarker.pose.position.y = am.pose.y;
    arucoMarker.pose.position.z = 0;
    arucoMarker.pose.orientation.x = 0.0;
    arucoMarker.pose.orientation.y = 0.0;
    arucoMarker.pose.orientation.z = sin(markerYaw / 2.0);
    arucoMarker.pose.orientation.w = cos(markerYaw / 2.0);
    arucoMarker.frame_locked = false;

    arucoMarkerText = arucoMarker;
    arucoMarkerArrow = arucoMarker;

    arucoMarker.scale.x = 0.02;
    arucoMarker.scale.y = 0.02;
    arucoMarker.scale.z = 0.001;
    arucoMarker.color.a = 0.5;
    arucoMarker.color.r = 255;
    arucoMarker.color.g = 255;
    arucoMarker.color.b = 255;

    arucoMarkerArray.markers.push_back(arucoMarker);

    arucoMarkerText.ns = ss.str() + "/text";
    arucoMarkerText.type = visualization_msgs::Marker::TEXT_VIEW_FACING;
    arucoMarkerText.text = ss.str();
    arucoMarkerText.scale.z = 0.004;
    arucoMarkerText.color.a = 1.0;
    arucoMarkerText.color.r = 0;
    arucoMarkerText.color.g = 1.0;
    arucoMarkerText.color.b = 1.0;

    arucoMarkerArray.markers.push_back(arucoMarkerText);

    arucoMarkerArrow.ns = ss.str() + "/arrow";
    arucoMarkerArrow.type = visualization_msgs::Marker::ARROW;
    arucoMarkerArrow.color.a = 0.5;
    arucoMarkerArrow.scale.x = 0.01;
    arucoMarkerArrow.scale.y = 0.0015;
    arucoMarkerArrow.scale.z = arucoMarkerArrow.scale.y;
    arucoMarkerArrow.color.r = 255;

    arucoMarkerArray.markers.push_back(arucoMarkerArrow);

    pubArucoVisualization_.publish(arucoMarkerArray);
}

void AccerionDriverROS::signatureMarkerStartStopAck(Acknowledgement ack)
{
    if (ack.value)
    {
        ROS_INFO("----------received start of map message!");

        mapPointCloud_.header.stamp = ros::Time::now();
        mapPointCloud_.height = 1;
        mapPointCloud_.width = 0;
        mapPointCloud_.data.resize(mapPointCloud_.width * mapPointCloud_.point_step);
    }
    else
    {
        ROS_INFO("----------received stop of map message!");
        ROS_INFO("number of markers in map: %d", mapPointCloud_.width);
        pubMapAsPC2_.publish(mapPointCloud_);
    }
}

void AccerionDriverROS::positionSetAck(Pose ps)
{
    ROS_INFO("ACK: new position is set!");
}

void AccerionDriverROS::consoleOutputAck(std::string output)
{
    ROS_INFO("ACK_CONSOLE_OUTPUT_INFO: %s", output.c_str());
}

void AccerionDriverROS::clusterRemovedAck(uint16_t clusterID)
{
    ROS_INFO("ACK_CLUSTER_REMOVED");
}

void AccerionDriverROS::lineFollowerModeAck(Acknowledgement ack)
{
    ROS_INFO("ACK_LINE_FOLLOWER_MODE");
}

void AccerionDriverROS::recoveryModeAck(Acknowledgement ack)
{
    ROS_INFO("ACK_RECOVERY_MODE");
}

void AccerionDriverROS::clusterLibraryRemovedAck(Acknowledgement ack)
{
    ROS_INFO("ACK_COMPLETE_CLUSTER_LIBRARY_REMOVED");
}

void AccerionDriverROS::idleModeAck(Acknowledgement ack)
{
    ROS_INFO("ACK_IDLE_MODE");
}

void AccerionDriverROS::localizationModeAck(Acknowledgement ack)
{
    ROS_INFO("ACK_LOCALIZATION_MODE");
}

void AccerionDriverROS::mappingModeAck(Acknowledgement ack)
{
    ROS_INFO("ACK_MAPPING_MODE");
}

void AccerionDriverROS::softwareVersionAck(SoftwareVersion sv)
{
    ROS_INFO("ACK_SOFTWARE_VERSION");
}

void AccerionDriverROS::softwareDetailsAck(SoftwareDetails sd)
{
    ROS_INFO("ACK_SOFTWARE_HASH %s %s", sd.softwareHash.c_str(), sd.date.c_str());
}

void AccerionDriverROS::mountPoseAck(Pose mp)
{
    ROS_INFO("Received Commands::ACK_SENSOR_MOUNT_POSE as x: %f y: %f th: %f", mp.x, mp.y, utils::deg2rad(mp.heading));

    geometry_msgs::TransformStamped outputToSensorTrans;

    outputToSensorTrans.transform.translation.x = mp.x;
    outputToSensorTrans.transform.translation.y = mp.y;
    outputToSensorTrans.transform.translation.z = 0;

    outputToSensorTrans.transform.rotation.x = 0;
    outputToSensorTrans.transform.rotation.y = 0;
    outputToSensorTrans.transform.rotation.z = sin(utils::deg2rad(mp.heading) / 2.0);
    outputToSensorTrans.transform.rotation.w = cos(utils::deg2rad(mp.heading) / 2.0);

    outputToSensorTrans.header.stamp = ros::Time::now();

    if (relTransOn_)
    {
        outputToSensorTrans.header.frame_id = relBaseFrame_;
        outputToSensorTrans.child_frame_id = relSensorFrame_;
        staticTfBroadcaster_.sendTransform(outputToSensorTrans);
    }

    if (absTransOn_)
    {
        outputToSensorTrans.header.frame_id = absBaseFrame_;
        outputToSensorTrans.child_frame_id = absSensorFrame_;
        staticTfBroadcaster_.sendTransform(outputToSensorTrans);
    }
}

void AccerionDriverROS::callbackResetPose(const geometry_msgs::PoseStamped::ConstPtr &resetPose)
{
    std::vector<uint8_t> dataToTransmit;

    ROS_INFO("new input reset pose: %f , %f , %f", resetPose->pose.position.x, resetPose->pose.position.y, getThetaFromQuat(resetPose->pose.orientation) * 180.0 / M_PI);
    Pose pose;
    pose.x = resetPose->pose.position.x;
    pose.y = resetPose->pose.position.y;
    pose.heading = getThetaFromQuat(resetPose->pose.orientation) * 100.0;

    // int32_t posX  = resetPose->pose.position.x*1e6;
    // int32_t posY  = resetPose->pose.position.y*1e6;
    // int32_t theta = getThetaFromQuat(resetPose->pose.orientation)*100.0*180.0/M_PI;

    // uint32_t uposX  = resetPose->pose.position.x*1e6;
    // uint32_t uposY  = resetPose->pose.position.y*1e6;
    // uint32_t utheta = getThetaFromQuat(resetPose->pose.orientation)*100.0*180.0/M_PI;

    accerionSensor_->setSensorPose(pose);
}

void AccerionDriverROS::callbackExternalReferencePose(const geometry_msgs::PoseWithCovarianceStampedPtr& ext_ref_msg)
{
    ext_ref_msg_ = *ext_ref_msg;
    uint64_t timeDiffUSec = 0;

    if (extRefPoseLatency_ == 0.0)
    {
        ros::Time timeNow = ros::Time::now();
        ros::Duration timeDifference;

        if (timeNow < ext_ref_msg->header.stamp)
        {
            ROS_WARN("Timestamp of the incoming ext reference pose message is in the future! Ignoring it and putting time offset to 0");
            timeDifference = ros::Duration(0.0);
        }
        else
        {
            timeDifference = timeNow - ext_ref_msg->header.stamp;
            if (timeDifference.toSec() > 1.0)
            {
                ROS_WARN("Time offset of the incoming ext reference pose message exceeds 1 second!");
            }
        }

        timeDiffUSec = (uint64_t)(timeDifference.toSec() * 1e6);

        // ROS_INFO("new input ext reference pose: %f [m], %f [m], %f [deg] with a time offset %.0f us",
        //             ext_ref_msg->pose.pose.position.x,
        //             ext_ref_msg->pose.pose.position.y,
        //             getThetaFromQuat(ext_ref_msg->pose.pose.orientation)*180.0/M_PI,
        //             (double)timeDiffUSec);
    }
    else
    {
        timeDiffUSec = extRefPoseLatency_ * 1e3;
    }

    // ROS_INFO("Inside callbackExternalReferencePose!");
    InputPose inputPose;
    inputPose.timeOffsetInUsec = timeDiffUSec;
    inputPose.pose.x = ext_ref_msg->pose.pose.position.x;
    inputPose.pose.y = ext_ref_msg->pose.pose.position.y;
    inputPose.pose.heading = getThetaFromQuat(ext_ref_msg->pose.pose.orientation);
    inputPose.standardDeviation.x = std::sqrt(ext_ref_msg->pose.covariance[0]);
    inputPose.standardDeviation.y = std::sqrt(ext_ref_msg->pose.covariance[7]);
    inputPose.standardDeviation.theta = std::sqrt(ext_ref_msg->pose.covariance[35]);
    accerionSensor_->setPoseAndCovariance(inputPose);
}

double AccerionDriverROS::getThetaFromQuat(const geometry_msgs::Quaternion quatIn)
{
    tf::Quaternion q(quatIn.x, quatIn.y, quatIn.z, quatIn.w);
    tf::Matrix3x3 m(q);
    double roll, pitch, yaw;
    m.getRPY(roll, pitch, yaw);

    if (yaw < -M_PI)
        yaw += 2.0 * M_PI;
    if (yaw > M_PI)
        yaw += -2.0 * M_PI;

    return yaw;
}

bool AccerionDriverROS::callbackServiceDeleteCluster(accerion_driver_msgs::Cluster::Request &req,
                                                     accerion_driver_msgs::Cluster::Response &res)
{
    int result = accerionSensor_->removeClusterFromLibraryBlocking(req.cluster_id);
    switch (result)
    {
    case -1:
        res.success = false;
        res.message = "A timeout occured when trying to delete a cluster. The acknowledgement was not retrieved, but the cluster might have been removed";
        break;
    case 0:
        res.success = false;
        res.message = "Cluster could not be deleted!";
        break;
    case 1:
        res.success = true;
        res.message = "Cluster succesfully deleted!";
        break;
    default:
        res.success = false;
        res.message = "Unknown return value...";
        break;
    }

    return true;
}

bool AccerionDriverROS::callbackServiceDeleteAllClusters(std_srvs::SetBool::Request &req,
                                                         std_srvs::SetBool::Response &res)
{
    int result = accerionSensor_->clearClusterLibraryBlocking();
    switch (result)
    {
    case -1:
        res.success = false;
        res.message = "A timeout occured when trying to delete all clusters. The acknowledgement was not retrieved, but the clusters might have been removed";
        break;
    case 0:
        res.success = false;
        res.message = "Clusters could not be deleted!";
        break;
    case 1:
        res.success = true;
        res.message = "Clusters succesfully deleted!";
        break;
    default:
        res.success = false;
        res.message = "Unknown return value...";
        break;
    }

    return true;
}

bool AccerionDriverROS::callbackServiceRecoveryMode(accerion_driver_msgs::SetRecoveryMode::Request &req,
                                                    accerion_driver_msgs::SetRecoveryMode::Response &res)
{
    int result = accerionSensor_->toggleRecoveryModeBlocking(req.recovery_mode, req.search_radius);
    switch (result)
    {
    case -1:
        res.success = false;
        res.msg = "A timeout occured while trying to toggle the recovery mode. Acknowledgement not received, but might have toggled";
        break;
    case 0:
    {
        res.success = false;
        if (req.recovery_mode)
        {
            res.msg = "Recovery mode not turned on";
        }
        else
        {
            res.msg = "Recovery mode not turned off";
        }
    }
    break;
    case 1:
    {
        res.success = true;
        if (req.recovery_mode)
        {
            res.msg = "Recovery mode succesfully turned on";
        }
        else
        {
            res.msg = "Recovery mode succesfully turned off";
        }
    }
    break;
    default:
        res.success = false;
        res.msg = "Unknown return value";
        break;
    }
    std::string msg = res.msg;
    ROS_INFO("%s", msg.c_str());
    return true;
}

bool AccerionDriverROS::callbackServiceMode(accerion_driver_msgs::ModeCommand::Request &req,
                                            accerion_driver_msgs::ModeCommand::Response &res)
{
    if (req.command.compare("start") != 0 && req.command.compare("stop") != 0)
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerMode_.getService() + " localization/idle/recording/reboot start/stop";
        return true;
    }
    bool start = true;
    if (req.command.compare("stop") == 0)
        start = false;

    int result = 0;
    if (req.mode.compare("localization") == 0)
    {
        result = accerionSensor_->toggleAbsoluteModeBlocking(start);
    }
    else if (req.mode.compare("idle") == 0)
    {
        result = accerionSensor_->toggleIdleModeBlocking(start);
    }
    else if (req.mode.compare("recording") == 0)
    {
        result = accerionSensor_->toggleRecordingModeBlocking(start);
    }
    else if (req.mode.compare("reboot") == 0)
    {
        result = accerionSensor_->toggleRebootModeBlocking(start);
    }
    else if (req.mode.compare("aruco") == 0)
    {
        result = accerionSensor_->toggleArucoMarkerDetectionModeBlocking(start);
    }
    else
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerMode_.getService() + " localization/idle/recording/reboot/aruco start/stop";
        return true;
    }
    switch (result)
    {
    case -1:
        res.success = false;
        res.message = "A timeout occured while trying to toggle. Acknowledgement not received, but might have toggled";
        break;
    case 0:
    {
        res.success = false;
        if (start)
        {
            res.message = "Mode not turned on";
        }
        else
        {
            res.message = "Mode not turned off";
        }
    }
    break;
    case 1:
    {
        res.success = true;
        if (start)
        {
            res.message = "Mode succesfully turned on";
        }
        else
        {
            res.message = "Mode succesfully turned off";
        }
    }
    break;
    default:
        res.success = false;
        res.message = "Unknown return value";
        break;
    }
    std::string msg = res.message;
    ROS_INFO("%s", msg.c_str());

    return true;
}

bool AccerionDriverROS::callbackServiceModeWithCluster(accerion_driver_msgs::ModeClusterCommand::Request &req,
                                                       accerion_driver_msgs::ModeClusterCommand::Response &res)
{
    if (req.command.compare("start") != 0 && req.command.compare("stop") != 0)
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerModeWithCluster_.getService() + " mapping/active_mapping/line_following 42 start/stop";
        return true;
    }

    bool start = true;
    if (req.command.compare("stop") == 0)
        start = false;

    int result = 0;
    if (req.mode.compare("mapping") == 0)
    {
        result = accerionSensor_->toggleMappingBlocking(start, req.cluster_id);
    }
    else if (req.mode.compare("line_following") == 0)
    {
        result = accerionSensor_->toggleLineFollowingBlocking(start, req.cluster_id);
    }
    else if (req.mode.compare("active_mapping") == 0)
    {
        result = accerionSensor_->toggleExpertModeBlocking(start);
        if (result == 1)
        {
            result = accerionSensor_->toggleMappingBlocking(start, req.cluster_id);
        }
    }
    else
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerModeWithCluster_.getService() + " mapping/active_mapping/line_following 42 start/stop";
        return true;
    }
    switch (result)
    {
    case -1:
        res.success = false;
        res.message = "A timeout occured while trying to toggle. Acknowledgement not received, but might have toggled";
        break;
    case 0:
    {
        res.success = false;
        if (start)
        {
            res.message = "Mode not turned on";
        }
        else
        {
            res.message = "Mode not turned off";
        }
    }
    break;
    case 1:
    {
        res.success = true;
        if (start)
        {
            res.message = "Mode succesfully turned on";
        }
        else
        {
            res.message = "Mode succesfully turned off";
        }
    }
    break;
    default:
        res.success = false;
        res.message = "Unknown return value";
        break;
    }
    std::string msg = res.message;
    ROS_INFO("%s", msg.c_str());

    return true;
}

bool AccerionDriverROS::callbackServiceRequest(accerion_driver_msgs::RequestCommand::Request &req,
                                               accerion_driver_msgs::RequestCommand::Response &res)
{
    if (req.request_command.compare("map") == 0)
    {
        accerionSensor_->requestMarkerMap();
    }
    else if (req.request_command.compare("sensor_mount_pose") == 0)
    {
        accerionSensor_->getSensorMountPose();
    }
    else if (req.request_command.compare("version") == 0)
    {
        accerionSensor_->getSoftwareVersion();
    }
    else if (req.request_command.compare("version_hash") == 0)
    {
        accerionSensor_->getSoftwareDetails();
    }
    else if (req.request_command.compare("ip_address") == 0)
    {
        accerionSensor_->getIPAddress();
    }
    else if (req.request_command.compare("serial_number") == 0)
    {
        SerialNumber sn = accerionSensor_->getSerialNumberBlocking();
        std::string serial = std::to_string(sn.serialNumber);
        res.message = serial;
        ROS_INFO("Current Serial Number: %s", serial.c_str());
    }
    else if (req.request_command.compare("correction_counter") == 0)
    {
        res.success = true;
        res.message = std::to_string(correctionCounter_);
    }
    else
    {
        res.success = false;
        res.message = "usage: rosservice call " + srvServerRequest_.getService() + " map/sensor_mount_pose/version/version_hash/ip_address/serial_number";
    }
    res.success = true;
    return true;
}

//ONLY USE FOR TESTING
bool AccerionDriverROS::callbackServiceResetPoseByTfLookup(accerion_driver_msgs::ParentChildFrames::Request &req,
                                                           accerion_driver_msgs::ParentChildFrames::Response &res)
{
    tf::StampedTransform currTf;

    try
    {
        double resetPosX = 0.0;
        double resetPosY = 0.0;
        double resetTheta = 0.0;

        ROS_INFO("looking up tf from %s to %s", req.parent.c_str(), req.child.c_str());
        tfListener_.lookupTransform(req.parent, req.child, ros::Time(0), currTf);

        ros::Time timeNow = ros::Time::now();
        // ROS_INFO("current time %ds %dns found tf at %ds %dns", timeNow.sec, timeNow.nsec, currTf.stamp_.sec, currTf.stamp_.nsec);
        ros::Duration timeDifference;

        if (timeNow < currTf.stamp_)
        {
            ROS_WARN("Timestamp of the looked up tf is in the future! Ignoring it and putting time offset to 0");
            timeDifference = ros::Duration(0.0);
        }
        else
        {
            timeDifference = timeNow - currTf.stamp_;
            if (timeDifference.toSec() > 1.0)
            {
                ROS_WARN("Time offset of the looked up tf exceeds 1 second!");
            }
        }

        uint64_t timeDiffUSec = (uint64_t)(timeDifference.toSec() * 1e6);

        tf::Quaternion tfQuat;
        tfQuat = currTf.getRotation();

        tf::Matrix3x3 m(tfQuat);
        double roll, pitch;
        m.getRPY(roll, pitch, resetTheta);

        tf::Vector3 tfVec;

        tfVec = currTf.getOrigin();

        resetPosX = tfVec.getX();
        resetPosY = tfVec.getY();

        ROS_INFO("new input reset pose: %f [m], %f [m], %f [deg] with a time offset %.0f us", resetPosX, resetPosY, resetTheta * 180.0 / M_PI, (double)timeDiffUSec);

        InputPose inputPose;
        inputPose.timeOffsetInUsec = timeDiffUSec;
        inputPose.pose.x = resetPosX;
        inputPose.pose.y = resetPosY;
        inputPose.pose.heading = resetTheta;
        inputPose.standardDeviation.x = 0;
        inputPose.standardDeviation.y = 0;
        inputPose.standardDeviation.theta = 0;
        accerionSensor_->setPoseAndCovariance(inputPose);

        // TODO: implement acknowledgement with request based comm with the sensor
        res.success = true;
        res.message = "Resetting pose by tf lookup from parent frame " + req.parent + " to child frame " + req.child + ". Note: no acknowledgement check is done. TODO";
    }
    catch (...)
    {
        ROS_INFO("%s: lookupTransform error! Does the tf from %s to %s exist?", nodeNs_.c_str(), req.parent.c_str(), req.child.c_str());
        res.success = true;
        res.message = "No tf could be found from parent frame " + req.parent + " to child frame " + req.child;
    }
    return true;
}

bool AccerionDriverROS::callbackServiceSetPose(accerion_driver_msgs::Pose::Request &req,
                                               accerion_driver_msgs::Pose::Response &res)
{
    int result = -1;
    Pose pose;
    pose.x = req.x;
    pose.y = req.y;
    pose.heading = req.theta;

    if (req.selection.compare("sensor_mount") == 0)
    {
        ROS_INFO("new input sensor_mount pose: %f , %f , %f", pose.x, pose.y, pose.heading);
        result = accerionSensor_->setSensorMountPoseBlocking(pose);
    }
    else if (req.selection.compare("sensor_output") == 0)
    {
        ROS_INFO("new input sensor_output pose: %f , %f , %f", pose.x, pose.y, pose.heading);
        result = accerionSensor_->setSensorPoseBlocking(pose);
    }
    else
    {
        res.success = false;
        res.message = "Unknown selection provided!";
        return false;
    }
    switch (result)
    {
    case -1:
        res.success = false;
        res.message = "A timeout occured while trying to set the pose. Acknowledgement not received, but might have been set";
        break;
    case 0:
        res.success = false;
        res.message = "Failed to set the desired position";
        break;
    case 1:
        res.success = true;
        res.message = "" + req.selection + " pose set to " + std::to_string(req.x) + "[m]," + std::to_string(req.y) + "[m]," + std::to_string(req.theta) + "[deg]";
        break;
    default:
        res.success = false;
        res.message = "Unknown return value";
        break;
    }
    std::string msg = res.message;
    ROS_INFO("%s", msg.c_str());
    return true;
}

bool AccerionDriverROS::callbackServiceSetInternalTracking(std_srvs::SetBool::Request &req,
                                                           std_srvs::SetBool::Response &res)
{
    int result = accerionSensor_->toggleInternalTrackingModeBlocking(req.data);
    switch (result)
    {
    case -1:
        res.success = false;
        res.message = "A timeout occured while trying to toggle the Internal Tracking mode. Acknowledgement not received, but might have toggled";
        break;
    case 0:
    {
        res.success = false;
        if (req.data)
        {
            res.message = "Internal Tracking mode not turned on";
        }
        else
        {
            res.message = "Internal Tracking mode not turned off";
        }
    }
    break;
    case 1:
    {
        res.success = true;
        if (req.data)
        {
            res.message = "Internal Tracking mode succesfully turned on";
        }
        else
        {
            res.message = "Internal Tracking mode succesfully turned off";
        }
    }
    break;
    default:
        res.success = false;
        res.message = "Unknown return value";
        break;
    }
    std::string msg = res.message;
    ROS_INFO("%s", msg.c_str());
    return true;
}

bool AccerionDriverROS::callbackServiceSetUDPSettings(accerion_driver_msgs::UDPSettings::Request &req,
                                                      accerion_driver_msgs::UDPSettings::Response &res)
{
    if ((req.message_type > 0 || req.message_type < 5) && (req.udp_mode == 1 || req.udp_mode == 2))
    {
        UDPInfo udpInfo;
        udpInfo.ipAddress.first = req.unicast_ip_address_first;
        udpInfo.ipAddress.second = req.unicast_ip_address_second;
        udpInfo.ipAddress.third = req.unicast_ip_address_third;
        udpInfo.ipAddress.fourth = req.unicast_ip_address_fourth;
        udpInfo.messageType = static_cast<EnabledMessageType>(req.message_type);
        udpInfo.strategy = static_cast<UDPStrategy>(req.udp_mode);
        int result = accerionSensor_->setUDPSettingsBlocking(udpInfo);
        switch (result)
        {
        case -1:
            res.success = false;
            res.message = "Timeout occurred while setting UDP settings, no acknowledgement retrieved, but settings might have been set.";
            break;
        case 0:
            res.success = false;
            res.message = "Failed to set UDP settings..";
            break;
        case 1:
            res.success = true;
            res.message = "Setting UDP settings to: unicastIp: " + std::to_string(req.unicast_ip_address_first) + "." + std::to_string(req.unicast_ip_address_second) + "." + std::to_string(req.unicast_ip_address_third) + "." + std::to_string(req.unicast_ip_address_fourth) + ", message type: " + std::to_string(req.message_type) + ", udp mode: " + std::to_string(req.udp_mode) + " . Note: no acknowledgement check is done. TODO";
            break;
        default:
            res.success = false;
            res.message = "Unknown return value..";
            break;
        }
    }
    else
    {
        res.success = false;
        res.message = "message type can be [1,2,3,4] and udp mode can be [1,2] only.";
    }

    // ROS_INFO("ACK_UDP_INFO: the sensor is in \n UDP %s mode \n message type %s \n unicast ip address %s ", std::to_string(req.udp_mode),std::to_string(req.message_type), req.unicast_ip_address);

    return true;
}

bool AccerionDriverROS::callbackServiceRecordings(accerion_driver_msgs::Recordings::Request &req,
                                                  accerion_driver_msgs::Recordings::Response &res)
{
    std::vector<std::string> recList;
    std::vector<uint8_t> indexes;
    for (int i = 0; i < req.recordingIndexes.size(); i++)
    {
        indexes.push_back(req.recordingIndexes[i]);
    }
    int result = 0;

    if (req.mode.compare("list") == 0)
    {
        result = accerionSensor_->getRecordingsListBlocking(recList);
        switch (result)
        {
        case -1:
            res.success = false;
            res.message = "A timeout occurred, the acknowledgement was not received, but operation might have succeeded..";
            break;
        case 0:
            res.success = false;
            res.message = "Operation failed..";
            break;
        case 1:
            res.recordings = recList;
            res.success = true;
            res.message = "Success..";
            break;
        default:
            res.success = false;
            res.message = "Unknown returncode..";
            break;
        }
    }
    else if (req.mode.compare("download") == 0)
    {
        auto progressLambda = [](int progress) {
            ROS_INFO("Recording retrieval progress: %i", progress);
        };

        auto doneLambda = [](bool done, std::string message) {
            ROS_INFO("%s", message.c_str());
        };

        auto statusLambda = [](FileSenderStatus status) {
            std::string statusMessage;
            switch (status)
            {
            case FileSenderStatus::ERROR_CODE_18:
                statusMessage = "Error Code 18...";
                break;
            case FileSenderStatus::ERROR_CODE_19:
                statusMessage = "Error Code 19...";
                break;
            case FileSenderStatus::ERROR_CODE_20:
                statusMessage = "Error Code 20...";
                break;
            case FileSenderStatus::CONNECTION_FAILED:
                statusMessage = "Connection Failed...";
                break;
            case FileSenderStatus::FAILED_TO_REMOVE_EXISTING:
                statusMessage = "Failed To Remove Existing...";
                break;
            case FileSenderStatus::FAILED_TO_OPEN_FILE:
                statusMessage = "Failed To Open File...";
                break;
            case FileSenderStatus::ALREADY_IN_PROGRESS:
                statusMessage = "Already In Progress...";
                break;
            case FileSenderStatus::PACKING_RECORDINGS:
                statusMessage = "Packing Recordings...";
                break;
            case FileSenderStatus::RETRIEVING_RECORDINGS:
                statusMessage = "Retrieving Recordings...";
                break;
            case FileSenderStatus::GATHERING_INFO:
                statusMessage = "Gathering Info...";
                break;
            case FileSenderStatus::TRANSFER_IN_PROGRESS:
                statusMessage = "Transfer In Progress...";
                break;
            default:
                statusMessage = "Unknown";
                break;
            }
            ROS_INFO("%s", statusMessage.c_str());
        };
        res.success = accerionSensor_->getRecordings(indexes, req.path, progressLambda, doneLambda, statusLambda);
        return true;
    }
    else if (req.mode.compare("delete") == 0)
    {
        DeleteRecordingsResult delresult = accerionSensor_->deleteRecordingsBlocking(indexes);
        if (delresult.success)
        {
            res.success = true;
            res.message = "Recordings deleted!";
        }
        else
        {
            res.success = false;
            std::stringstream failedIndexesString;
            failedIndexesString << "The following recordings could not be deleted: ";
            for (auto fi : delresult.failedIndexes)
            {
                failedIndexesString << fi << ", ";
            }
            res.message = failedIndexesString.str();
        }
        return true;
    }
    else
    {
        res.success = false;
        res.message = "Available options are list, download and delete";
        return false;
    }
}

bool AccerionDriverROS::callbackServiceUpdate(accerion_driver_msgs::FileTransfer::Request &req,
                           accerion_driver_msgs::FileTransfer::Response &res)
{
    Address localIP;
    localIP.first = 0;
    localIP.second = 0;
    localIP.third = 0;
    localIP.fourth = 0;
    if(updateService_ == nullptr)
    {
        updateService_ = updateManager_->getAccerionUpdateServiceBySerial(std::to_string(sensorSerial_), localIP);
        sleep(1);
    }
    if (updateService_ == nullptr)
    {
        res.message = "Could not connect to UpdateService";
        res.success = false;
        return false;
    }

    auto progressLambda = [](int progress) {
        ROS_INFO("Update Transfer progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };

    auto statusLambda = [](FileSenderStatus status) {
        std::string statusMessage;
        switch (status)
        {
        case FileSenderStatus::SENDING_UPDATE:
            statusMessage = "Sending Update...";
            break;
        case FileSenderStatus::UNPACKING_AND_UPDATING:
            statusMessage = "Unpacking And Updating...";
            break;
        case FileSenderStatus::FAILED_TO_STOP:
            statusMessage = "Failed To Stop...";
            break;
        case FileSenderStatus::FAILED_TO_START:
            statusMessage = "Failed To Start...";
            break;
        case FileSenderStatus::FAILED_TO_BACKUP:
            statusMessage = "Failed To Backup...";
            break;
        case FileSenderStatus::FAILED_TO_UNZIP_UPDATE:
            statusMessage = "Failed To Unzip Update...";
            break;
        case FileSenderStatus::FAILED_TO_RESTORE_BACKUP:
            statusMessage = "Failed To Restore Backup...";
            break;
        case FileSenderStatus::ERROR_CODE_18:
            statusMessage = "Error Code 18...";
            break;
        case FileSenderStatus::ERROR_CODE_19:
            statusMessage = "Error Code 19...";
            break;
        case FileSenderStatus::ERROR_CODE_20:
            statusMessage = "Error Code 20...";
            break;
        case FileSenderStatus::CONNECTION_FAILED:
            statusMessage = "Connection Failed...";
            break;
        case FileSenderStatus::FAILED_TO_REMOVE_EXISTING:
            statusMessage = "Failed To Remove Existing...";
            break;
        case FileSenderStatus::FAILED_TO_OPEN_FILE:
            statusMessage = "Failed To Open File...";
            break;
        case FileSenderStatus::ALREADY_IN_PROGRESS:
            statusMessage = "Already In Progress...";
            break;
        case FileSenderStatus::GATHERING_INFO:
            statusMessage = "Gathering Info...";
            break;
        case FileSenderStatus::TRANSFER_IN_PROGRESS:
            statusMessage = "Transfer In Progress...";
            break;
        default:
            statusMessage = "Unknown";
            break;
        }
        ROS_INFO("%s", statusMessage.c_str());
    };
    res.success = updateService_->updateSensor(req.path, progressLambda, doneLambda, statusLambda);
    res.message = "Update request done!";
    return true;
}

bool AccerionDriverROS::callbackServiceLogs(accerion_driver_msgs::Logs::Request &req,
                                accerion_driver_msgs::Logs::Response &res)
{
    Address localIP;
    localIP.first = 0;
    localIP.second = 0;
    localIP.third = 0;
    localIP.fourth = 0;
    if(updateService_ == nullptr)
    {
        updateService_ = updateManager_->getAccerionUpdateServiceBySerial(std::to_string(sensorSerial_), localIP);
        sleep(1);
    }
    if (updateService_ == nullptr)
    {
        res.message = "Could not connect to UpdateService";
        res.success = false;
        return false;
    }

    auto progressLambda = [](int progress) {
        ROS_INFO("Retrieving Logs progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };

    auto statusLambda = [](FileSenderStatus status) {
        std::string statusMessage;
        switch (status)
        {
        case FileSenderStatus::PACKING_LOGS:
            statusMessage = "Packing Logs...";
            break;
        case FileSenderStatus::ERROR_CODE_18:
            statusMessage = "Error Code 18...";
            break;
        case FileSenderStatus::ERROR_CODE_19:
            statusMessage = "Error Code 19...";
            break;
        case FileSenderStatus::ERROR_CODE_20:
            statusMessage = "Error Code 20...";
            break;
        case FileSenderStatus::RETRIEVING_LOGS:
            statusMessage = "Retrieving Logs...";
            break;
        case FileSenderStatus::CONNECTION_FAILED:
            statusMessage = "Connection Failed...";
            break;
        case FileSenderStatus::FAILED_TO_REMOVE_EXISTING:
            statusMessage = "Failed To Remove Existing...";
            break;
        case FileSenderStatus::FAILED_TO_OPEN_FILE:
            statusMessage = "Failed To Open File...";
            break;
        case FileSenderStatus::ALREADY_IN_PROGRESS:
            statusMessage = "Already In Progress...";
            break;
        case FileSenderStatus::GATHERING_INFO:
            statusMessage = "Gathering Info...";
            break;
        case FileSenderStatus::TRANSFER_IN_PROGRESS:
            statusMessage = "Transfer In Progress...";
            break;
        default:
            statusMessage = "Unknown";
            break;
        }
        ROS_INFO("%s", statusMessage.c_str());
    };
    if(req.backupLogs)
    {
        res.success = updateService_->getBackupLogs(req.path, progressLambda, doneLambda, statusLambda);
    }
    else
    {
        res.success = updateService_->getLogs(req.path, progressLambda, doneLambda, statusLambda);  
    }
    res.message = "Get Logs request done!";
    return true;
}

bool AccerionDriverROS::callbackServiceGetMap(accerion_driver_msgs::FileTransfer::Request &req,
                                              accerion_driver_msgs::FileTransfer::Response &res)
{
    auto progressLambda = [](int progress) {
        ROS_INFO("Map retrieval progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };

    auto statusLambda = [](FileSenderStatus status) {
        std::string statusMessage;
        switch (status)
        {
        case FileSenderStatus::PACKING_MAP:
            statusMessage = "Packing Map...";
            break;
        case FileSenderStatus::ERROR_CODE_18:
            statusMessage = "Error Code 18...";
            break;
        case FileSenderStatus::ERROR_CODE_19:
            statusMessage = "Error Code 19...";
            break;
        case FileSenderStatus::ERROR_CODE_20:
            statusMessage = "Error Code 20...";
            break;
        case FileSenderStatus::RETRIEVING_MAP:
            statusMessage = "Retrieving Map...";
            break;
        case FileSenderStatus::CONNECTION_FAILED:
            statusMessage = "Connection Failed...";
            break;
        case FileSenderStatus::FAILED_TO_REMOVE_EXISTING:
            statusMessage = "Failed To Remove Existing...";
            break;
        case FileSenderStatus::FAILED_TO_OPEN_FILE:
            statusMessage = "Failed To Open File...";
            break;
        case FileSenderStatus::ALREADY_IN_PROGRESS:
            statusMessage = "Already In Progress...";
            break;
        case FileSenderStatus::GATHERING_INFO:
            statusMessage = "Gathering Info...";
            break;
        case FileSenderStatus::TRANSFER_IN_PROGRESS:
            statusMessage = "Transfer In Progress...";
            break;
        default:
            statusMessage = "Unknown";
            break;
        }
        ROS_INFO("%s", statusMessage.c_str());
    };
    res.success = accerionSensor_->getMap(req.path, progressLambda, doneLambda, statusLambda);
    return true;
}

bool AccerionDriverROS::callbackServiceSendMap(accerion_driver_msgs::MapTransfer::Request &req,
                                accerion_driver_msgs::MapTransfer::Response &res)
{
 auto progressLambda = [](int progress) {
        ROS_INFO("Map transfer progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };

    auto statusLambda = [](FileSenderStatus status) {
        std::string statusMessage;
        switch (status)
        {
        case FileSenderStatus::SENDING_MAP:
            statusMessage = "Sending Map...";
            break;
        case FileSenderStatus::UNPACKING_AND_REPLACING_MAP:
            statusMessage = "Unpacking And Replacing Map...";
            break;
        case FileSenderStatus::ERROR_CODE_18:
            statusMessage = "Error Code 18...";
            break;
        case FileSenderStatus::ERROR_CODE_19:
            statusMessage = "Error Code 19...";
            break;
        case FileSenderStatus::ERROR_CODE_20:
            statusMessage = "Error Code 20...";
            break;
        case FileSenderStatus::CONNECTION_FAILED:
            statusMessage = "Connection Failed...";
            break;
        case FileSenderStatus::FAILED_TO_REMOVE_EXISTING:
            statusMessage = "Failed To Remove Existing...";
            break;
        case FileSenderStatus::FAILED_TO_OPEN_FILE:
            statusMessage = "Failed To Open File...";
            break;
        case FileSenderStatus::ALREADY_IN_PROGRESS:
            statusMessage = "Already In Progress...";
            break;
        case FileSenderStatus::GATHERING_INFO:
            statusMessage = "Gathering Info...";
            break;
        case FileSenderStatus::TRANSFER_IN_PROGRESS:
            statusMessage = "Transfer In Progress...";
            break;
        default:
            statusMessage = "Unknown";
            break;
        }
        ROS_INFO("%s", statusMessage.c_str());
    };
    if(req.merge)//0 = replace 1 = merge
    {
        res.success = accerionSensor_->sendMap(req.path, progressLambda, doneLambda, statusLambda, 1);
    }
    else
    {
        res.success = accerionSensor_->sendMap(req.path, progressLambda, doneLambda, statusLambda, 0);
    }
    return true;
}

bool AccerionDriverROS::callbackServiceGetG2O(accerion_driver_msgs::FileTransfer::Request &req,
                                accerion_driver_msgs::FileTransfer::Response &res)
{
    auto progressLambda = [](int progress) {
        ROS_INFO("G2O File retrieval progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };

    auto statusLambda = [](FileTransferStatus status) {
        std::string statusMessage;
        switch (status)
        {
        case FileTransferStatus::FILE_NOT_FOUND:
            statusMessage = "File not found..";
            break;
        case FileTransferStatus::FILE_READ_FAILURE:
            statusMessage = "File read failure..";
            break;
        case FileTransferStatus::FILE_WRITE_FAILURE:
            statusMessage = "File write failure..";
            break;
        case FileTransferStatus::FILE_TRANSFER_ALREADY_IN_PROGRESS:
            statusMessage = "File transfer already in progress..";
            break;
        case FileTransferStatus::FILE_REMOVAL_FAILURE:
            statusMessage = "File removal failure..";
            break;
        case FileTransferStatus::FILE_GATHERING_INFO:
            statusMessage = "Gathering file info..";
            break;
        case FileTransferStatus::FILE_TRANSFER_IN_PROGRESS:
            statusMessage = "File transfer in progress..";
            break;
        default:
            statusMessage = "Unknown";
            break;
        }
        ROS_INFO("%s", statusMessage.c_str());
    };
    res.success = accerionSensor_->getG2OFile(req.path, progressLambda, doneLambda, statusLambda);
    return true;
}

bool AccerionDriverROS::callbackServiceSendG2O(accerion_driver_msgs::FileTransfer::Request &req,
                                accerion_driver_msgs::FileTransfer::Response &res)
{
    auto progressLambda = [](int progress) {
        ROS_INFO("G2O File sending progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };

    auto statusLambda = [](FileTransferStatus status) {
        std::string statusMessage;
        switch (status)
        {
        case FileTransferStatus::FILE_NOT_FOUND:
            statusMessage = "File not found..";
            break;
        case FileTransferStatus::FILE_READ_FAILURE:
            statusMessage = "File read failure..";
            break;
        case FileTransferStatus::FILE_WRITE_FAILURE:
            statusMessage = "File write failure..";
            break;
        case FileTransferStatus::FILE_TRANSFER_ALREADY_IN_PROGRESS:
            statusMessage = "File transfer already in progress..";
            break;
        case FileTransferStatus::FILE_REMOVAL_FAILURE:
            statusMessage = "File removal failure..";
            break;
        case FileTransferStatus::FILE_GATHERING_INFO:
            statusMessage = "Gathering file info..";
            break;
        case FileTransferStatus::FILE_TRANSFER_IN_PROGRESS:
            statusMessage = "File transfer in progress..";
            break;
        default:
            statusMessage = "Unknown";
            break;
        }
        ROS_INFO("%s", statusMessage.c_str());
    };
    res.success = accerionSensor_->sendG2OFile(req.path, progressLambda, doneLambda, statusLambda);
    return true;
}

bool AccerionDriverROS::callbackServiceImportG2O(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response)
{
    auto progressLambda = [](int progress) {
        ROS_INFO("Import G2O progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };
    accerionSensor_->importG2OFile(progressLambda, doneLambda);
    return true;
}

bool AccerionDriverROS::callbackServiceSearchLoopClosures(std_srvs::SetBool::Request  &req,
                                                        std_srvs::SetBool::Response &res)
{
    auto progressLambda = [](int progress) {
        ROS_INFO("Search loop closure progress: %i", progress);
    };

    auto doneLambda = [](bool done, std::string message) {
        ROS_INFO("%s", message.c_str());
    };
    res.success = true;
    accerionSensor_->toggleSearchForLoopClosures(req.data, progressLambda, doneLambda);
    return true;
}

bool AccerionDriverROS::callbackServiceSetIPAddress(accerion_driver_msgs::SetIP::Request  &req,
                                                    accerion_driver_msgs::SetIP::Response &res)
{
    std::string fullIP = req.ip;
    std::string fullNetmask = req.netmask;
    std::string fullGateway = req.gateway;

    std::vector<std::string> ipStrings, netmaskStrings, gatewayStrings;
    std::istringstream fullIPStream(fullIP), fullNetmaskStream(fullNetmask), fullGatewayStream(fullGateway);
    std::string s;    
    while (getline(fullIPStream, s, '.')) {
        ipStrings.push_back(s);
    }
    while (getline(fullNetmaskStream, s, '.')) {
        netmaskStrings.push_back(s);
    }
    while (getline(fullGatewayStream, s, '.')) {
        gatewayStrings.push_back(s);
    }
    if(ipStrings.size() != 4 || netmaskStrings.size() != 4 || gatewayStrings.size() != 4)
    {
        res.success = false;
        res.message = "One of the addresses was invalid..";
        return false;
    }
    struct sockaddr_in ipAddr, netmaskAddr, gatewayAddr;
#ifdef _WIN32 
    int ipResult      = InetPton(AF_INET, fullIP.c_str(), &(ipAddr.sin_addr));
    int netmaskResult = InetPton(AF_INET, fullNetmask.c_str(), &(netmaskAddr.sin_addr));
    int gatewayResult = InetPton(AF_INET, fullGateway.c_str(), &(gatewayAddr.sin_addr));
#else
    int ipResult      = inet_pton(AF_INET, fullIP.c_str(), &(ipAddr.sin_addr));
    int netmaskResult = inet_pton(AF_INET, fullNetmask.c_str(), &(netmaskAddr.sin_addr));
    int gatewayResult = inet_pton(AF_INET, fullGateway.c_str(), &(gatewayAddr.sin_addr));
#endif
    if(ipResult != 1 || netmaskResult != 1 || gatewayResult != 1)
    {
        res.success = false;
        res.message = "One of the addresses was invalid..";
        return false;
    }
    IPAddress ipStruct;
    ipStruct.ipAddress.first    = static_cast<uint8_t>(std::stoi(ipStrings[0]));
    ipStruct.ipAddress.second   = static_cast<uint8_t>(std::stoi(ipStrings[1]));
    ipStruct.ipAddress.third    = static_cast<uint8_t>(std::stoi(ipStrings[2]));
    ipStruct.ipAddress.fourth   = static_cast<uint8_t>(std::stoi(ipStrings[3]));

    ipStruct.netmask.first      = static_cast<uint8_t>(std::stoi(netmaskStrings[0]));
    ipStruct.netmask.second     = static_cast<uint8_t>(std::stoi(netmaskStrings[1]));
    ipStruct.netmask.third      = static_cast<uint8_t>(std::stoi(netmaskStrings[2]));
    ipStruct.netmask.fourth     = static_cast<uint8_t>(std::stoi(netmaskStrings[3]));

    ipStruct.gateway.first      = static_cast<uint8_t>(std::stoi(gatewayStrings[0]));
    ipStruct.gateway.second     = static_cast<uint8_t>(std::stoi(gatewayStrings[1]));
    ipStruct.gateway.third      = static_cast<uint8_t>(std::stoi(gatewayStrings[2]));
    ipStruct.gateway.fourth     = static_cast<uint8_t>(std::stoi(gatewayStrings[3]));

    IPAddressExtended result = accerionSensor_->setIPAddressBlocking(ipStruct);
    res.success = true;
    res.message = "Check output to verify settings";
    ROS_INFO("static ip address  : %i.%i.%i.%i", result.staticIPAddress.first, result.staticIPAddress.second, result.staticIPAddress.third, result.staticIPAddress.fourth);
    ROS_INFO("static netmask     : %i.%i.%i.%i", result.staticNetmask.first, result.staticNetmask.second, result.staticNetmask.third, result.staticNetmask.fourth);
    ROS_INFO("dynamic ip address : %i.%i.%i.%i", result.dynamicIPAddress.first, result.dynamicIPAddress.second, result.dynamicIPAddress.third, result.dynamicIPAddress.fourth);
    ROS_INFO("dynamic netmask    : %i.%i.%i.%i", result.dynamicNetmask.first, result.dynamicNetmask.second, result.dynamicNetmask.third, result.dynamicNetmask.fourth);
    ROS_INFO("default gateway    : %i.%i.%i.%i", result.defaultGateway.first, result.defaultGateway.second, result.defaultGateway.third, result.defaultGateway.fourth);
    return true;
}

void AccerionDriverROS::ipAddressAck(IPAddressExtended ip)
{
    ROS_INFO("static ip address  : %i.%i.%i.%i", ip.staticIPAddress.first,  ip.staticIPAddress.second,  ip.staticIPAddress.third,   ip.staticIPAddress.fourth);
    ROS_INFO("static netmask     : %i.%i.%i.%i", ip.staticNetmask.first,    ip.staticNetmask.second,    ip.staticNetmask.third,     ip.staticNetmask.fourth);
    ROS_INFO("dynamic ip address : %i.%i.%i.%i", ip.dynamicIPAddress.first, ip.dynamicIPAddress.second, ip.dynamicIPAddress.third,  ip.dynamicIPAddress.fourth);
    ROS_INFO("dynamic netmask    : %i.%i.%i.%i", ip.dynamicNetmask.first,   ip.dynamicNetmask.second,   ip.dynamicNetmask.third,    ip.dynamicNetmask.fourth);
    ROS_INFO("default gateway    : %i.%i.%i.%i", ip.defaultGateway.first,   ip.defaultGateway.second,   ip.defaultGateway.third,    ip.defaultGateway.fourth);
}

bool AccerionDriverROS::callbackServiceSecondaryLinefollower(accerion_driver_msgs::SecondaryLF::Request  &req,
                                        accerion_driver_msgs::SecondaryLF::Response &res)
{
    LineFollowerData lfd = accerionSensor_->getSecondaryLineFollowerOutputBlocking(req.clusterID);
    if(lfd.timeStamp == 0 && lfd.pose.x == 0 && lfd.pose.y == 0 && lfd.pose.heading == 0 && lfd.closestPointX == 0 && lfd.closestPointY == 0 && lfd.reserved == 0 && lfd.clusterID == 0)
    {
        res.success = false;
        res.message = "A timeout occurred";
        return false;
    }
    else
    {
        res.success         = true;
        res.message         = "Response received";
        res.timeStamp       = lfd.timeStamp;
        res.x               = lfd.pose.x;
        res.y               = lfd.pose.y;
        res.heading         = lfd.pose.heading;
        res.closestPointX   = lfd.closestPointX;
        res.closestPointY   = lfd.closestPointY;
        res.reserved        = lfd.reserved;
        res.clusterID       = lfd.clusterID;
        return true;   
    }
}

bool AccerionDriverROS::callbackServiceSetDateTime(accerion_driver_msgs::DateTime::Request  &req,
                                        accerion_driver_msgs::DateTime::Response &res)
{
    std::string fullDate = req.date;
    std::string fullTime = req.time;

    std::vector<std::string> dateStrings, timeStrings;
    std::istringstream fullDateStream(fullDate), fullTimeStream(fullTime);
    std::string s;    
    while (getline(fullDateStream, s, '/')) {
        dateStrings.push_back(s);
    }
    while (getline(fullTimeStream, s, ':')) {
        timeStrings.push_back(s);
    }
    if(dateStrings.size() != 3 || timeStrings.size() != 3)
    {
        res.success = false;
        res.message = "One of the fields was invalid..";
        return false;
    }

    DateTime dt;
    dt.day = static_cast<uint8_t>(std::stoi(dateStrings[0]));
    dt.month = static_cast<uint8_t>(std::stoi(dateStrings[1]));
    dt.year = static_cast<uint16_t>(std::stoi(dateStrings[2]));

    dt.hours = static_cast<uint8_t>(std::stoi(timeStrings[0]));
    dt.minutes = static_cast<uint8_t>(std::stoi(timeStrings[1]));
    dt.seconds = static_cast<uint8_t>(std::stoi(timeStrings[2]));
    int result = accerionSensor_->setDateTimeBlocking(dt);
    switch (result)
    {
        case -1:
            res.success = false;
            res.message = "A timeout occurred. Datetime might have been set, but no acknowledgement was received..";
            break;
        case 0:
            res.success = false;
            res.message = "Failed to set datetime";
            break;
        case 1:
            res.success = true;
            res.message = "Datetime set";
            break;
        default:
            res.success = false;
            res.message = "Unknown returnvalue..";
            break;
    }
    return res.success;
}
